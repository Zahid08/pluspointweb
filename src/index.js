import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import { ScrollContext } from "react-router-scroll-4";
import { IntlReducer as Intl, IntlProvider } from "react-redux-multilingual";
import "./index.scss";
import API from "./config";

// Import custom components
import store from "./store";
import translations from "./constants/translations";
import { getAllProducts } from "./actions";

// Layouts
import Vegetables from "./components/layouts/vegetables/main";
import outlet from "./components/layouts/vegetables/outlet";

//Collection Pages
import CollectionLeftSidebar from "./components/collection/collection-left-sidebar";

// Product Pages
import LeftSideBar from "./components/products/left-sidebar";
import LeftImage from "./components/products/left-image";

// Features
import Layout from "./components/app";
import Cart from "./components/cart";
import Compare from "./components/compare/index";
import wishList from "./components/wishlist";
import WishListFrontend from "./components/wishlist/wishlist";
import checkOut from "./components/checkout";
import Paysenz from "./components/paysenz/paysenz";
import orderSuccess from "./components/checkout/success-page";

// Extra Pages
import aboutUs from "./components/pages/about-us";
import CustomerService from "./components/pages/customer-service";
import TermsConditions from "./components/pages/terms-conditions";
import DashboardLeft from "./components/pages/dashboard-left";
import Privacy from "./components/pages/privacy";
import PageNotFound from "./components/pages/404";
import Login from "./components/pages/login";
import Register from "./components/pages/register";
import ForgetPassword from "./components/pages/forget-password";
import Contact from "./components/pages/contact";
import Dashboard from "./components/pages/dashboard";
import ThankYou from "./components/pages/thank-you";
import ChangePass from "./components/pages/change-pass";
import ForgetPasswordByEmail from "./components/pages/forget-password-by-email";
import MyOrders from "./components/pages/my-orders";

// Blog Pages
import RightSide from "./components/blogs/right-sidebar";
import Details from "./components/blogs/details";


class Root extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      allDataList: []
    };
  }
  componentDidMount() {
    this.getFullProductList();
    setTimeout(() => {
      localStorage.removeItem('token');
      localStorage.removeItem('username');
      localStorage.removeItem('custId');
      localStorage.removeItem('author_id');
    }, 30 * 60 * 1000);
  }

  getFullProductList = async () => {
    let res = await fetch(API.products, {
      method: "GET",
      cache: "no-cache"
    }).then(response => {
      return response.json();
    });
    this.setState({ allDataList: res.data });
  };

  render() {
    if(this.state.allDataList.length>0) {
      store.dispatch(getAllProducts(this.state.allDataList));
    }

    return (
      <Provider store={store}>
        <IntlProvider translations={translations} locale="en">
          <BrowserRouter basename={"/"}>
            <ScrollContext>
              <Switch>
                <Route exact path={`${process.env.PUBLIC_URL}/`} component={Vegetables} />
                {/*<Route path={`${process.env.PUBLIC_URL}/vegetables`} component={Vegetables}/>*/}
                <Layout>
                  <Route exact path={`${process.env.PUBLIC_URL}/products/:category/:subcategory`} component={CollectionLeftSidebar} />
                  <Route exact path={`${process.env.PUBLIC_URL}/products/:category/:subcategory/:childsubcategory`} component={CollectionLeftSidebar} />
                  <Route exact path={`${process.env.PUBLIC_URL}/products/:category`} component={CollectionLeftSidebar} />

                  <Route exact path={`${process.env.PUBLIC_URL}/product/:id`} component={LeftImage} />

                  {/*Routes For custom Features*/}
                  <Route path={`${process.env.PUBLIC_URL}/cart`} component={Cart} />
                  <Route path={`${process.env.PUBLIC_URL}/wishlist`} component={wishList} />
                  <Route path={`${process.env.PUBLIC_URL}/wishlist-checkout`} component={WishListFrontend} />
                  <Route path={`${process.env.PUBLIC_URL}/compare`} component={Compare} />
                  <Route path={`${process.env.PUBLIC_URL}/checkout`} component={checkOut} />
                  <Route path={`${process.env.PUBLIC_URL}/paysenz-order`} component={Paysenz} />
                  <Route exact path={`${process.env.PUBLIC_URL}/order-success/:orderid`} component={orderSuccess} />

                  <Route path={`${process.env.PUBLIC_URL}/sales/orders`} component={aboutUs} />

                  {/*Routes For Extra Pages*/}
                  <Route path={`${process.env.PUBLIC_URL}/pages/about-us`} component={aboutUs} />
                  <Route path={`${process.env.PUBLIC_URL}/pages/customer-service`} component={CustomerService} />
                  <Route path={`${process.env.PUBLIC_URL}/pages/terms-conditions`} component={TermsConditions} />
                  <Route path={`${process.env.PUBLIC_URL}/pages/dashboard-left`} component={DashboardLeft} />
                  <Route path={`${process.env.PUBLIC_URL}/pages/privacy`} component={Privacy} />
                  <Route path={`${process.env.PUBLIC_URL}/pages/404`} component={PageNotFound} />
                  <Route path={`${process.env.PUBLIC_URL}/login`} component={Login} />
                  <Route path={`${process.env.PUBLIC_URL}/register`} component={Register} />
                  <Route path={`${process.env.PUBLIC_URL}/forget-password`} component={ForgetPassword} />
                  <Route path={`${process.env.PUBLIC_URL}/pages/contact`} component={Contact} />
                  <Route path={`${process.env.PUBLIC_URL}/dashboard`} component={Dashboard} />
                  <Route path={`${process.env.PUBLIC_URL}/pages/thank-you`} component={ThankYou} />
                  <Route path={`${process.env.PUBLIC_URL}/change-pass`} component={ChangePass} />
                  <Route path={`${process.env.PUBLIC_URL}/forget-password-by-email`} component={ForgetPasswordByEmail} />
                  <Route path={`${process.env.PUBLIC_URL}/my-orders`} component={MyOrders} />
                  <Route exact path={`${process.env.PUBLIC_URL}/outlet`} render={() => (<Redirect to="/"/>)}/>
                  <Route path={`${process.env.PUBLIC_URL}/outlet/:divison`} component={outlet} />
                  {/*Blog Pages*/}
                  <Route path={`${process.env.PUBLIC_URL}/blog/right-sidebar`} component={RightSide} />
                  <Route path={`${process.env.PUBLIC_URL}/blog/details`} component={Details} />

                  {/* <Route exact path="*" component={PageNotFound} /> */}
                </Layout>
              </Switch>
            </ScrollContext>
          </BrowserRouter>
        </IntlProvider>
      </Provider>
    );
  }
}

ReactDOM.render(<Root />, document.getElementById("root"));
