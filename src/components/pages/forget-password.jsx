import React, {Component} from 'react';
import Breadcrumb from "../common/breadcrumb";
import API from "../../config";
import {toast} from "react-toastify";

class ForgetPassword extends Component {

    constructor (props) {
        super (props);
        this.state={
            register_email: "",
            message: ""
        };
        this.register_email = this.register_email.bind(this);
        this.Registration = this.Registration.bind(this);
    }   

    register_email(event) {
        this.setState({ register_email: event.target.value });        
    }

    Registration(event) {
        fetch(API.ForgetPassword, {
        method: "post",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            "X-Requested-With": "XMLHttpRequest"
        },
        body: JSON.stringify({        
            email: this.state.register_email
        })
        })
        .then(Response => Response.json())
        .then(result => this.setState({message: "Please check your email...!"}));
    }

    render (){

        console.log(this.state.message);
        return (
            <div>
                <Breadcrumb title={'forget password'}/>
                
                
                {/*Forget Password section*/}
                <section className="pwd-page section-b-space">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6 offset-lg-3">
                                <h2>Forget Your Password ?</h2>
                                <form className="theme-form">
                                    <div className="form-row">
                                        <div className="col-md-12">
                                            <input type="text" className="form-control" id="email"
                                                   placeholder="Enter Your Email" required=""  onChange={this.register_email}/>
                                        </div>
                                        <a className="btn btn-solid" style={{color: "#fff"}} onClick={this.Registration}>Submit</a>
                                    </div>
                                </form>
                                <p style={{color: "red"}}>{this.state.message}</p>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        )
    }
}

export default ForgetPassword