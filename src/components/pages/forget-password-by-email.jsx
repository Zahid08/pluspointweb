import React, { Component } from "react";
import Breadcrumb from "../common/breadcrumb";
import { connect } from "react-redux";
import DashboardLeft from "./dashboard-left";
import API from "../../config";

class ForgetPasswordByEmail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Password: "",
      ConfirmPassword: "",
      ConfirmPassword_msg: ""
    };
    this.ConfirmPassword = this.ConfirmPassword.bind(this);
    this.Password = this.Password.bind(this);
    this.login = this.login.bind(this);
  }

  Password(event) {    
    this.setState({ Password: event.target.value });    
  }


  ConfirmPassword(event) {    
    this.setState({ ConfirmPassword: event.target.value });    
  }


  componentDidMount(){
    //alert(this.state.Password);
  }

  login(event) { 

    if(this.state.Password==this.state.ConfirmPassword){
      let customerId=localStorage.getItem('custId');
      if(customerId && this.state.Password){

        fetch(API.ForgetPassword, {
          method: "post",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            "X-Requested-With": "XMLHttpRequest"
          },
          body: JSON.stringify({
            user_id:customerId,
            new_password:this.state.Password
          })
        }).then(Response => Response.json())
          .then(result => {
            if(result.status==200){
                localStorage.removeItem('token');
                localStorage.removeItem('username');
                localStorage.removeItem('custId');
                localStorage.removeItem('author_id');
            }
          });
      }
    }else{
      this.setState({ConfirmPassword_msg: "Please enter corret password"})
    }    
  }
  render() {
    console.log('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA');
    console.log(this.props.match.params);

    const { language } = this.props;
    var tokenCheck = localStorage.getItem("token");
    if (tokenCheck) {
    } else {
    }
    return (
      <div>
        <Breadcrumb title={"Reset Password"} />
        {/*Dashboard section*/}
        <section className="section-b-space">
          <div className="container">
            <div className="row">              
              <div className="col-lg-12">
                <div className="dashboard-right">
                  {/*Forget Password section*/}
                  <section className="pwd-page" style={{ paddingTop: 0 }}>
                    <div className="container">
                      <div className="row">
                        <div className="col-lg-6 offset-lg-3">
                          <h2>{language === "en" ? "Reset your password" : "আপনার পাসওয়ার্ড পরিবর্তন করুন"}</h2>
                          <form className="theme-form">
                            <div className="form-row">
                              <div className="col-md-12">                                
                                <input onChange={this.Password} type="text" className="form-control" id="new" placeholder="Enter Your New Password" required="" />
                                <input onChange={this.ConfirmPassword} type="text" className="form-control" id="new" placeholder="Enter Your Confirm Password" required="" />
                                <span style={{color: "red", marginTop: "0", marginBottom: "20px", display: "block"}}>{this.state.ConfirmPassword_msg}</span>
                              </div>
                              <a style={{color:"#fff"}} className="btn btn-solid" onClick={this.login}>
                              {language === "en" ? "Submit" : "লগইন"}
                            </a>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </section>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  language: state.Intl.locale
});

export default connect(mapStateToProps)(ForgetPasswordByEmail);
