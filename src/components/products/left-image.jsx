import React, { Component, lazy, Suspense  } from "react";
import "../common/index.scss";
import { connect } from "react-redux";
import Slider from "react-slick";
//import ReactImageMagnify from "react-image-magnify";

import ReactImageAppear from 'react-image-appear';
import Loader from 'react-loader-spinner';


// import custom Components
import RelatedProduct from "../common/related-product";
import Breadcrumb from "../common/breadcrumb";
import DetailsWithPrice from "./common/product/details-price";
import DetailsTopTabs from "./common/details-top-tabs";
import { addToCart, addToCartUnsafe, addToWishlist } from "../../actions";
import image1 from "./02.png";



const ReactImageMagnify = lazy(()=>{
  return new Promise(resolve => {
    setTimeout(() => resolve(import("react-image-magnify")), 5000);
  });
});

// const ReactImageMagnify = lazy(()=>import('react-image-magnify'));

class LeftImage extends Component {
  constructor() {
    super();
    this.state = {
      nav1: null,
      nav2: null,
      vertical: true,
      image: image1,
      singleImage: false,
      secondImage: "",
      loader: ''
    };
  }

  
  componentWillMount() {
    if (window.innerWidth > 576) {
      this.setState({ vertical: true });
    } else {
      this.setState({ vertical: false });
    }
  }

  componentDidMount() {
    this.setState({
      nav1: this.slider1,
      nav2: this.slider2
    });
  }

  componentDidUpdate(){
   
  }

    handelImage = (imageValue) => {
    let ImagePath='';
    if (imageValue==''){
      ImagePath=`${process.env.PUBLIC_URL}/assets/images/no-preview-image.png`;
     }else {
      ImagePath=imageValue;
    }
    console.log(ImagePath);
      this.setState({secondImage: ImagePath});
    }

  render() {
    const { item, addToCart, addToCartUnsafe, addToWishlist } = this.props;

    let ZoomingImage='';
    if (this.props.item) {
      if (item.variation_type == 1 && item.imageSize.length>0) {
        ZoomingImage = item.imageSize[0].large;
      }else {
        ZoomingImage=`${process.env.PUBLIC_URL}/assets/images/no-preview-image.png`;
      }
    }

    var productsnav = {
      vertical: this.state.vertical,
      verticalSwiping: this.state.vertical,
      slidesToShow: 6,
      slidesToScroll: 1,
      asNavFor: ".product-right-slick",
      arrows: false,
      infinite: true,
      centerMode: false,
      dots: false,
      focusOnSelect: true,
      responsive: [
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 6,
            slidesToScroll: 1
          }
        }
      ]
    };
    return (
      <div>
        {item ? <Breadcrumb parent={"Product"} title={item.name} /> : ""}
        {item ? (
          <section>
            <div className="collection-wrapper">
              <div className="container">
                <div className="row">
                  <div className="col-lg-1 col-sm-2 col-xs-12 p-0">
                    {/* <SmallImages item={item} settings={productsnav} navOne={this.state.nav1} /> */}
                    <div className="row">
                      <div className="col-12 p-0">
                        <Slider
                          {...productsnav}
                          asNavFor={this.props.navOne}
                          ref={slider => (this.slider2 = slider)}
                          className="slider-nav"
                        >
                          {item.variants
                            ? item.variants.map((vari, index) => (
                                <div key={index}>
                                  <img src={`${vari.images}`} key={index} alt="" className="img-fluid" />
                                </div>
                              ))
                            : item.pictures.map((vari, index) => (
                                <div key={index}>
                                  <img
                                    onClick={() => {
                                      this.setState({ secondImage: item.imageSize[`${index}`].large });
                                    }}
                                    src={`${vari}`}
                                    key={index}
                                    alt=""
                                    className="img-fluid"
                                  />
                                </div>
                              ))}
                        </Slider>
                      </div>
                    </div>
                  </div>              
                  <div className="col-lg-5 col-sm-10 col-xs-12  order-up" style={{margin: "auto 0", textAlign: "center"}}>  
                                    
                  <Suspense fallback={
                  <div><Loader
                    type="Circles"
                    color="#000"
                    height={100}
                    width={100}
                    timeout={5000}
                  /></div>}>
                    {item && (
                      <ReactImageMagnify
                        {...{
                          smallImage: {
                            alt: "",
                            isFluidWidth: true,
                            src: this.state.secondImage.length < 1 ? ZoomingImage : this.state.secondImage
                          },
                          largeImage: {
                            src: this.state.secondImage.length < 1 ? ZoomingImage : this.state.secondImage,
                            width: 1300,
                            height: 1800,
                            onLoad: this.state.secondImage.length < 1 ? ZoomingImage : this.state.secondImage
                          },
                          enlargedImagePosition: "over"                        
                        }}
                      />
                    )}
                    </Suspense>
                  </div>
                  <DetailsWithPrice
                    item={item}
                    navOne={this.state.nav1}
                    addToCartClicked={addToCart}
                    BuynowClicked={addToCartUnsafe}
                    addToWishlistClicked={addToWishlist}
                    onSelectImage={this.handelImage}
                  />
                </div>
              </div>
            </div>
          </section>
        ) : (
          ""
        )}
        {/*Section End*/}

        {/* Review Section starts */}
        {item ? (
          <section className="tab-product m-0">
            <div className="container">
              <div className="row">
                <div className="col-sm-12 col-lg-12">
                  <DetailsTopTabs item={item} />
                </div>
              </div>
            </div>
          </section>
        ) : (
          ""
        )}
        {/* Review Section ends */}
        <RelatedProduct />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  let productId = ownProps.match.params.id;
  return {
    item: state.data.products.find(el => el.id === productId),
    symbol: state.data.symbol
  };
};

export default connect(mapStateToProps, {
  addToCart,
  addToCartUnsafe,
  addToWishlist
})(LeftImage);
