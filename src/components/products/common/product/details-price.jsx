import React, { Component } from "react";
import { Link } from "react-router-dom";
import Slider from "react-slick";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { addToCart } from "../../../../actions";
import _ from "lodash";
import API from "../../../../config";
import CartPage from "../../../../components/common/headers/common/cart-header";

import {
  FacebookShareButton,
  FacebookIcon,
  TwitterShareButton,
  TwitterIcon,
  EmailShareButton,
  EmailIcon,
  LinkedinShareButton,
  LinkedinIcon
} from "react-share";
import {toast} from "react-toastify";

class DetailsWithPrice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: 1,
      size: "",
      color: "",
      currentSalePrice:"",
      currentItemCode:"",
      currentDiscount:"",
      stockStatus: "InStock",
      filteredCurrentStock: null,
      nav3: null,
      reviewCount: null,
      uniqueColors: [],
      uniqueSize: []
    };
  }

  componentDidMount() {
    const { item } = this.props;
    //if veriation created
    if (item.variation_type === 2 && item.variation.length>0) {
      this.setState({ color: item.variation[0].color });
      this.setState({ size: item.variation[0].size });
      const uniqueColors = _.uniqBy(item.variation, "color").map(item => {
        return item.color;
      });
      const uniqueSize = _.uniqBy(item.variation, "size").map(item => {
        return item.size;
      });
      this.setState({ uniqueColors }, this.setState({ uniqueSize }, this.calculateCurrentStock));
    } else {
      this.setState({ filteredCurrentStock: item.stock });
    }

    this.setState({
      nav3: this.slider3
    });

    //Fetch Review Content
    fetch(API.item_review, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      }
    })
      .then(resp => resp.json())
      .then(response => {
        if(response.data.status==200){
        const productReview = response.data.filter(review => this.props.item.id == review.itemId);
        this.setState({ reviewCount: productReview.length });
        }
      });
  }

  //minusQty contatity return
  minusQty = () => {
    if (this.state.quantity > 1) {
      this.setState({ stockStatus: "InStock" });
      this.setState({ quantity: this.state.quantity - 1 });
    }
  };

  plusQty = () => {
    let {quantity, filteredCurrentStock } = this.state;
    if (this.props.item.variation_type !=2){
      if (this.props.item.stock >= this.state.quantity) {
        this.setState({ quantity: this.state.quantity + 1 });
      } else {
        this.setState({ stockStatus: "Out of Stock 111111!" });
      }
    }else {
      if (filteredCurrentStock - quantity >= 0) {
        this.setState({ quantity: this.state.quantity + 1 });
      } else {
        this.setState({ stockStatus: "Out of Stock !" });
      }
    }
  };

  changeQty = e => {
    this.setState({ quantity: parseInt(e.target.value) });
  };



  handleAddToCart = event => {
    let { item } = this.props;    
    let { color, size, quantity, filteredCurrentStock } = this.state;
    
    let getSttaus=false;
    if(this.props.cartItems.length>0){
      let cartItemsSerach=this.props.cartItems.find(e1=>e1.id==item.id);
      if(cartItemsSerach){
      if(cartItemsSerach.qty>filteredCurrentStock ||(filteredCurrentStock-cartItemsSerach.qty)<quantity){
        getSttaus=true;
      }
    }
    }
 
    let variation;
    if (filteredCurrentStock - quantity >= 0 && getSttaus==false){
      if (item.variation_type != 2) {
        this.props.addToCart(item, quantity,'','','','');
      } else {
        if (item.variation.length>0){
          variation = item.variation.find(item => {
            if (item.color == color && item.size == size) {
              return item;
            }
          });
          if(variation){
            this.props.addToCart(variation, quantity, color, size, variation.id,variation.image);
          }
        }
        else {
          toast.error("ERROR");
        }
      }
    } else {
      this.setState({ stockStatus: "Out of Stock !" });
      toast.error("Out of Stock !");
    }
  };

  calculateCurrentStock = () => {
    //this.props.onSelectImage(`${process.env.PUBLIC_URL}/assets/images/no-preview-image.png`);
    const { color, size } = this.state;
    const { item } = this.props;

    //back to child component Image
    if (item.variation_type == 2) {
      //Get Only Colour WIse Image Path
      let variationItems = item.variation.find(item => {
        if (item.color == color) {
          return item;
        }
      });
      this.props.onSelectImage(`${variationItems.image.large}`);

       //Get Colour and size wise Price and discount
      let priceGet = item.variation.find(item => {
        if (item.color == color && item.size == size) {
          return item;
        }
      });

      let ItemCodeGet = item.variation.find(item => {
        if (item.size == size) {
          return item;
        }
      });


      //item Code set
      if(typeof ItemCodeGet=='undefined'){
        this.setState({ currentItemCode: item.itemCode});
      }else{
        this.setState({ currentItemCode: ItemCodeGet.item_code});
      }

      if(typeof priceGet=='undefined'){
        this.setState({ currentSalePrice: item.salePrice });
        this.setState({ currentDiscount: item.discount });
      }else{
        console.log(priceGet);
        this.setState({ currentSalePrice: priceGet.sell_price });
        this.setState({ currentDiscount: priceGet.discount });
      }

    }
    
    if (color === "" && size === "") {
      const totalStock = item.variation.reduce((sum, item) => {
        return sum + item.quantity;
      }, 0);
      if (totalStock==0){
        this.setState({ stockStatus: "Out of Stock !" });
      }else {
        this.setState({ stockStatus: "In Stock !" });
      }
      this.setState({ filteredCurrentStock: totalStock });
      return;
    } else {
      const totalStock = item.variation.reduce((sum, item) => {
        if (item.color == (this.state.color || "") && item.size == (this.state.size || "")) {
          return sum + item.quantity;
        } else {
          return sum;
        }
      }, 0);
      if (totalStock==0){
        this.setState({ stockStatus: "Out of Stock !" });
      }else {
        this.setState({ stockStatus: "In Stock !" });
      }
      this.setState({ filteredCurrentStock: totalStock });
    }
  };

  render() {
    const { symbol, item, addToCartClicked, BuynowClicked, addToWishlistClicked, language,onSelectImage, cartItems } = this.props;
    const { reviewCount, uniqueSize, uniqueColors, filteredCurrentStock, quantity,currentSalePrice,currentDiscount,currentItemCode } = this.state;


    var colorsnav = {
      slidesToShow: 6,
      swipeToSlide: true,
      arrows: false,
      dots: false,
      focusOnSelect: true
    };

    return (
      <div className="col-lg-6 rtl-text">
        <Helmet>
          <title>
            Pluspoint | {item.category} | {item.name}
          </title>
        </Helmet>
        <div className="product-right">
          <span style={{ background: "red", marginRight: "5px" }} className="in-stock-btn">
            -{currentDiscount}%
          </span>
          <span className="in-stock-btn">{this.state.stockStatus}</span>
          <h2> {language == "en" ? item.name : item.name_bd} </h2>
          <h4> {currentItemCode?currentItemCode:item.itemCode} </h4>
          <div className="single-page-rating">
            <i className="fa fa-star"></i>
            <i className="fa fa-star"></i>
            <i className="fa fa-star"></i>
            <i className="fa fa-star"></i>
            <i className="fa fa-star"></i>
            {reviewCount && (
              <span>
                {" "}
                {reviewCount} {reviewCount > 1 ? "Reviews" : "Review"}
              </span>
            )}
          </div>

          {
            currentDiscount || item.discount?
                <div className="all-price">
                  <del>
                    {symbol}
                    {/* {item.salePrice} */}
                    {currentSalePrice?currentSalePrice:item.sell_price}
                  </del>{" "}
                  <h3>
                    {symbol}
                    {/* {item.salePrice - (item.salePrice * item.discount) / 100}{" "} */}
                    {currentSalePrice?currentSalePrice - (currentSalePrice* currentDiscount) / 100:item.sell_price - (item.sell_price * item.discount) / 100}
                  </h3>
                </div>:
                <div className="all-price">
                  <h3>
                    {symbol}
                    {currentSalePrice?currentSalePrice:item.sell_price}
                  </h3>
                </div>
          }
          <div className="save-percentage">
            {language == "en" ? "You Save:" : "আপনি সংরক্ষণ করুন:"}{" "}
            <span>
              {" "}
              {symbol}
              {/* {(item.salePrice * item.discount) / 100} */}
              {currentSalePrice?(currentSalePrice* currentDiscount) / 100:(item.sell_price * item.discount) / 100}
            </span>
            <span> ({currentDiscount}% off)</span>
          </div>
          <div className="product-description border-product">
            {item.variation_type === 2 ? (
              <div>
                <h6 className="product-title size-text">{language == "en" ? "Size" : "আকার"}</h6>

                <div className="size-box">
                  <ul>
                    {uniqueSize.map((s, i) => {
                      if (s.toString() == this.state.size) {
                        return (
                          <li className="front-check-single-active"
                            value={s.toString()}
                            onClick={e => {
                              this.setState({ size: e.currentTarget.getAttribute("value") }, this.calculateCurrentStock);
                            }}
                            key={i}
                            id={i}
                          >
                            {s}
                          </li>
                        );
                      } else {
                        return (
                          <li className="front-check-single"
                            value={s.toString()}
                            onClick={e => {
                              this.setState({ size: e.currentTarget.getAttribute("value") }, this.calculateCurrentStock);
                            }}
                            key={i}
                            id={i}
                          >
                            {s}
                          </li>
                        );
                      }
                    })}
                  </ul>
                </div>
              </div>
            ) : (
              ""
            )}
            <button type="button" className="btn size-btn" data-toggle="modal" data-target="#exampleModalCenter">
              {language == "en" ? "Size Chart" : "মাপের তালিকা"}
            </button>
            <div>
              {item.variation_type==2 && <h6 className="product-title size-text">{language == "en" ? "Color" : "রং"}</h6>}
              <div className="size-box">
                <ul>
                  {uniqueColors.map(color => {
                    if (color.length > 1) {
                      if (color == this.state.color) {
                        return (
                          <li
                            value={color.toString()}
                            onClick={e => {
                              // console.log(e.currentTarget.getAttribute("value"));
                              this.setState({ color: e.currentTarget.getAttribute("value") }, this.calculateCurrentStock);
                            }}
                            style={{
                              background: `${color}`,
                              width: "25px",
                              height: "25px",
                              border: "3px solid #fff",
                              boxShadow: "0 0 0 3px hsl(0, 0%, 80%)"
                            }}
                          ></li>
                        );
                      } else {
                        return (
                          <li
                            value={color.toString()}
                            onClick={e => {
                              // console.log(e.currentTarget.getAttribute("value"));
                              this.setState({ color: e.currentTarget.getAttribute("value") }, this.calculateCurrentStock);
                            }}
                            style={{ background: `${color}`, width: "25px", height: "25px", border: "1px solid #000" }}
                          ></li>
                        );
                      }
                    }
                  })}
                </ul>
              </div>
            </div>
            <div
              className="modal fade"
              id="exampleModalCenter"
              tabindex="-1"
              role="dialog"
              aria-labelledby="exampleModalCenterTitle"
              aria-hidden="true"
            >
              <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLongTitle">
                      {language == "en" ? "Size Chart" : "মাপের তালিকা"}
                    </h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <img src={`${process.env.PUBLIC_URL}/assets/images/size-chart.png`} alt="" className="img-fluid" />
                  </div>
                </div>
              </div>
            </div>
            <h6 className="product-title">{language == "en" ? "quantity" : "পরিমাণ"}</h6>
            <div className="qty-box">
              <div className="input-group">
                <span className="input-group-prepend">
                  <button type="button" className="btn quantity-left-minus" onClick={this.minusQty} data-type="minus" data-field="">
                    <i className="fa fa-minus"></i>
                  </button>
                </span>
                <input readOnly
                  type="text"
                  name="quantity"
                  value={this.state.quantity}
                  onChange={this.changeQty}
                  className="form-control input-number"
                />
                <span className="input-group-prepend">
                  <button type="button" className="btn quantity-right-plus" onClick={this.plusQty} data-type="plus" data-field="">
                    <i className="fa fa-plus"></i>
                  </button>
                </span>
              </div>
            </div>
            <p style={{ margin: "10px 0" }}>
              {language == "en" ? "Hurry! Only" : "তাড়াতাড়ি! কেবল"}{" "}
              <span style={{ color: "red", fontWeight: "bold" }}>{filteredCurrentStock}</span>
              {language == "en" ? " Left in Stock!" : "স্টক মধ্যে বাকি!"}
            </p>
            <div className="progress">
              <div
                className="progress-bar"
                role="progressbar"
                style={{ width: `${filteredCurrentStock}%` }}
                aria-valuenow={item.stock}
                aria-valuemin="0"
                aria-valuemax="100"
              ></div>
            </div>
            <p style={{ marginTop: "10px" }}>
              {language == "en"
                ? "Order in the next 24 Hours to get it by Tuesday 17/2/2020"
                : "মঙ্গলবার 17/2/2020 এর মধ্যে পরবর্তী 24 ঘন্টা অর্ডার করুন"}
            </p>
          </div>
          <div className="product-buttons">
            <div className="row">
              <div className="col-md-12">
                <a className="btn btn-solid btn-cart " onClick={this.handleAddToCart}>
                  {language == "en" ? "add to cart" : "কার্ট যোগ করুন"}
                </a>
              </div>
              <div className="col-md-6">
                <a className="btn btn-solid btn-wishlist" onClick={() => addToWishlistClicked(item)}>
                  {language == "en" ? "add to wishlist" : "চাহিদাপত্রে যোগ করা"}
                </a>
              </div>
              <div className="col-md-6">
                <a className="btn btn-solid btn-compare" onClick={() => addToCartClicked(item, this.state.quantity)}>
                  {language == "en" ? "add to compare" : "তুলনা যোগ করুন"}
                </a>
              </div>
              <div className="col-md-12">
                <Link
                  to={`${process.env.PUBLIC_URL}/checkout`}
                  className="btn btn-solid btn-buy"
                  onClick={() => BuynowClicked(item, this.state.quantity)}
                >
                  {language == "en" ? "buy now" : "এখন কিনুন"}
                </Link>
              </div>
              <div className="col-md-12">
                <img className="img-fluid" src={`${process.env.PUBLIC_URL}/assets/images/payment-getwaye.png`} alt="user" />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  language: state.Intl.locale,
  symbol: state.data.symbol,
  cartItems: state.cartList.cart
});

export default connect(mapStateToProps, { addToCart })(DetailsWithPrice);
