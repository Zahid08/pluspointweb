import React, { Component } from 'react';
import Slider from 'react-slick';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux'

import {getSingleItem, getSpecialCollection} from '../../../services/index'
import {
    addToCart,
    addToWishlist,
    addToCompare,
    incrementQty,
    decrementQty,
    removeFromCart
} from "../../../actions/index";
import ProductItem from './special-product-item';

class Special extends Component {

    render (){

        const {product, symbol, addToCart, addToWishlist, addToCompare, incrementQty, decrementQty, removeFromCart} = this.props;

        return (
            <div>
                {/*Paragraph*/}
                <section className="section-b-space addtocart_count">
                    <div className="full-box">
                        <div className="container">
                            <div className="title4">
                                <h2 className="title-inner4">Top Selling of the week</h2>
                                <h2 className="view-all" style={{float: "right"}}><a href="#">View All</a></h2>
                                <div className="line"><span></span></div>
                            </div>
                            <div className="row">
                                <div className="col-md-3 center-slider">
                                    <div>
                                        <div className="offer-slider">
                                            <div>
                                                <ProductItem product={product[2]} symbol={symbol}
                                                             onAddToCompareClicked={() => addToCompare(product[2])}
                                                             onAddToWishlistClicked={() => addToWishlist(product[2])}
                                                             onAddToCartClicked={() => addToCart(product[2], 1)}
                                                             onIncrementClicked={() => incrementQty(product[2], 1)}
                                                             onDecrementClicked={() => decrementQty(product[2].id)}
                                                             onRemoveFromCart={() => removeFromCart(product[2])}  />
                                            </div>
                                            <div style={{textAlign: "center"}} className="rating"><i className="fa fa-star"></i> <i
                                                className="fa fa-star"></i> <i
                                                className="fa fa-star"></i> <i
                                                className="fa fa-star"></i> <i
                                                className="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 center-slider">
                                    <div>
                                        <div className="offer-slider">
                                            <div>
                                                <ProductItem product={product[2]} symbol={symbol}
                                                             onAddToCompareClicked={() => addToCompare(product[2])}
                                                             onAddToWishlistClicked={() => addToWishlist(product[2])}
                                                             onAddToCartClicked={() => addToCart(product[2], 1)}
                                                             onIncrementClicked={() => incrementQty(product[2], 1)}
                                                             onDecrementClicked={() => decrementQty(product[2].id)}
                                                             onRemoveFromCart={() => removeFromCart(product[2])}  />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 center-slider">
                                    <div>
                                        <div className="offer-slider">
                                            <div>
                                                <ProductItem product={product[2]} symbol={symbol}
                                                             onAddToCompareClicked={() => addToCompare(product[2])}
                                                             onAddToWishlistClicked={() => addToWishlist(product[2])}
                                                             onAddToCartClicked={() => addToCart(product[2], 1)}
                                                             onIncrementClicked={() => incrementQty(product[2], 1)}
                                                             onDecrementClicked={() => decrementQty(product[2].id)}
                                                             onRemoveFromCart={() => removeFromCart(product[2])}  />
                                            </div>
                                            <div style={{textAlign: "center"}} className="rating"><i className="fa fa-star"></i> <i
                                                className="fa fa-star"></i> <i
                                                className="fa fa-star"></i> <i
                                                className="fa fa-star"></i> <i
                                                className="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 center-slider">
                                    <div>
                                        <div className="offer-slider">
                                            <div>
                                                <ProductItem product={product[2]} symbol={symbol}
                                                             onAddToCompareClicked={() => addToCompare(product[2])}
                                                             onAddToWishlistClicked={() => addToWishlist(product[2])}
                                                             onAddToCartClicked={() => addToCart(product[2], 1)}
                                                             onIncrementClicked={() => incrementQty(product[2], 1)}
                                                             onDecrementClicked={() => decrementQty(product[2].id)}
                                                             onRemoveFromCart={() => removeFromCart(product[2])}  />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 center-slider">
                                    <div>
                                        <div className="offer-slider">
                                            <div>
                                                <ProductItem product={product[2]} symbol={symbol}
                                                             onAddToCompareClicked={() => addToCompare(product[2])}
                                                             onAddToWishlistClicked={() => addToWishlist(product[2])}
                                                             onAddToCartClicked={() => addToCart(product[2], 1)}
                                                             onIncrementClicked={() => incrementQty(product[2], 1)}
                                                             onDecrementClicked={() => decrementQty(product[2].id)}
                                                             onRemoveFromCart={() => removeFromCart(product[2])}  />
                                            </div>
                                            <div style={{textAlign: "center"}} className="rating"><i className="fa fa-star"></i> <i
                                                className="fa fa-star"></i> <i
                                                className="fa fa-star"></i> <i
                                                className="fa fa-star"></i> <i
                                                className="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 center-slider">
                                    <div>
                                        <div className="offer-slider">
                                            <div>
                                                <ProductItem product={product[2]} symbol={symbol}
                                                             onAddToCompareClicked={() => addToCompare(product[2])}
                                                             onAddToWishlistClicked={() => addToWishlist(product[2])}
                                                             onAddToCartClicked={() => addToCart(product[2], 1)}
                                                             onIncrementClicked={() => incrementQty(product[2], 1)}
                                                             onDecrementClicked={() => decrementQty(product[2].id)}
                                                             onRemoveFromCart={() => removeFromCart(product[2])}  />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 center-slider">
                                    <div>
                                        <div className="offer-slider">
                                            <div>
                                                <ProductItem product={product[2]} symbol={symbol}
                                                             onAddToCompareClicked={() => addToCompare(product[2])}
                                                             onAddToWishlistClicked={() => addToWishlist(product[2])}
                                                             onAddToCartClicked={() => addToCart(product[2], 1)}
                                                             onIncrementClicked={() => incrementQty(product[2], 1)}
                                                             onDecrementClicked={() => decrementQty(product[2].id)}
                                                             onRemoveFromCart={() => removeFromCart(product[2])}  />
                                            </div>
                                            <div style={{textAlign: "center"}} className="rating"><i className="fa fa-star"></i> <i
                                                className="fa fa-star"></i> <i
                                                className="fa fa-star"></i> <i
                                                className="fa fa-star"></i> <i
                                                className="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 center-slider">
                                    <div>
                                        <div className="offer-slider">
                                            <div>
                                                <ProductItem product={product[2]} symbol={symbol}
                                                             onAddToCompareClicked={() => addToCompare(product[2])}
                                                             onAddToWishlistClicked={() => addToWishlist(product[2])}
                                                             onAddToCartClicked={() => addToCart(product[2], 1)}
                                                             onIncrementClicked={() => incrementQty(product[2], 1)}
                                                             onDecrementClicked={() => decrementQty(product[2].id)}
                                                             onRemoveFromCart={() => removeFromCart(product[2])}  />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

const mapStateToProps = (state, Ownprops) => ({
    product: getSpecialCollection(state.data.products, Ownprops.type),
    symbol: state.data.symbol
})

export default connect(mapStateToProps,
    {
        addToCart,
        addToWishlist,
        addToCompare,
        incrementQty,
        decrementQty,
        removeFromCart
    }) (Special);