import React, { Component } from "react";
import { Link ,Redirect  } from "react-router-dom";
import Modal from "react-responsive-modal";
import connect from "react-redux/lib/connect/connect";
import _ from "lodash";
import {toast} from "react-toastify";
import {addToCart} from "../../../actions";


import Slider from "react-slick";
import ReactImageMagnify from "react-image-magnify";
import image1 from "../../products/02.png";
import "../../../../src/index.scss";

class ProductItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      stock: "InStock",
      quantity: 1,
      image: "",
      size: "",
      color: "",
      currentSalePrice:"",
      currentDiscount:"",
      stockStatus: "InStock",
      filteredCurrentStock: null,
      nav3: null,
      reviewCount: null,
      uniqueColors: [],
      uniqueSize: [],
      secondImage: "",
      vertical:true
    };
  }

  componentDidMount() {
    const { product } = this.props;
    if (product.variation_type === 2 && product.variation.length>0) {
      this.setState({ color: product.variation[0].color });
      this.setState({ size: product.variation[0].size });
      const uniqueColors = _.uniqBy(product.variation, "color").map(product => {
        return product.color;
      });
      const uniqueSize = _.uniqBy(product.variation, "size").map(product => {
        return product.size;
      });
      this.setState({ uniqueColors }, this.setState({ uniqueSize }, this.calculateCurrentStock));
    } else {
      this.setState({ filteredCurrentStock: product.stock });
    }
  }

  onClickHandle(img) {
    this.setState({ image: img });
  }
  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  minusQty = () => {
    if (this.state.quantity > 1) {
      this.setState({ stock: "InStock" });
      this.setState({ quantity: this.state.quantity - 1 });
    }
  };

  plusQty = () => {
    let {quantity, filteredCurrentStock } = this.state;
    if (this.props.product.variation_type !=2){
      if (this.props.product.stock >= this.state.quantity) {
        this.setState({ quantity: this.state.quantity + 1 });
      } else {
        this.setState({ stockStatus: "Out of Stock !" });
      }
    }else {
      if (filteredCurrentStock - quantity >= 0) {
        this.setState({ quantity: this.state.quantity + 1 });
      } else {
        this.setState({ stockStatus: "Out of Stock !" });
      }
    }
  };


  changeQty = e => {
    this.setState({ quantity: parseInt(e.target.value) });
  };

  calculateCurrentStock = () => {
    //this.props.onSelectImage(`${process.env.PUBLIC_URL}/assets/images/no-preview-image.png`);
    const { color, size } = this.state;
    const { product } = this.props;

    //back to child component Image
    if (product.variation_type == 2) {
      let variationItems = product.variation.find(product => {
        if (product.color.toLowerCase() == color.toLowerCase()) {
          return product;
        }
      });

      
       //Get Colour and size wise Price and discount
       let priceGet = product.variation.find(product => {
        if (product.color == color && product.size == size) {
          return product;
        }
      });

      if(typeof priceGet=='undefined'){
        this.setState({ currentSalePrice: product.salePrice });
        this.setState({ currentDiscount: product.discount });
      }else{
        console.log(priceGet);
        this.setState({ currentSalePrice: priceGet.sell_price });
        this.setState({ currentDiscount: priceGet.discount });
      }


      
      let  ImagePath='';
      if (variationItems.image==''){
        ImagePath=`${process.env.PUBLIC_URL}/assets/images/no-preview-image.png`;
      }else {
        ImagePath=variationItems.image.medium;
      }
      this.setState({secondImage: ImagePath});
      //this.props.onSelectImage(`${variationItems.image}`);
    }

    if (color === "" && size === "") {
      const totalStock = product.variation.reduce((sum, product) => {
        return sum + product.quantity;
      }, 0);
      if (totalStock==0){
        this.setState({ stockStatus: "Out of Stock !" });
      }else {
        this.setState({ stockStatus: "In Stock !" });
      }
      this.setState({ filteredCurrentStock: totalStock });
      return;
    } else {
      const totalStock = product.variation.reduce((sum, product) => {
        if (product.color == (this.state.color || "") && product.size == (this.state.size || "")) {
          return sum + product.quantity;
        } else {
          return sum;
        }
      }, 0);
      if (totalStock==0){
        this.setState({ stockStatus: "Out of Stock !" });
      }else {
        this.setState({ stockStatus: "In Stock !" });
      }
      this.setState({ filteredCurrentStock: totalStock });
    }
  };


  handleAddToCart = event => {
    let { product } = this.props;
    let { color, size, quantity, filteredCurrentStock } = this.state;

    let getSttaus=false;
    if(this.props.cartItems.length>0){
      let cartItemsSerach=this.props.cartItems.find(e1=>e1.id==product.id);
      if(cartItemsSerach){
      if(cartItemsSerach.qty>filteredCurrentStock ||(filteredCurrentStock-cartItemsSerach.qty)<quantity){
        getSttaus=true;
      }
    }
    }

    let variation;
    if (filteredCurrentStock - quantity >= 0 && getSttaus==false) {
      if (product.variation_type != 2) {
        this.onCloseModal();
        this.props.addToCart(product, quantity,'','','','');
      } else {
        if (product.variation.length>0){
          variation = product.variation.find(product => {
            if (product.color == color && product.size == size) {
              return product;
            }
          });
          this.onCloseModal();
          if(variation){
            this.props.addToCart(variation, quantity, color, size, variation.id,variation.image);
          }
        }
        else {
          toast.error("ERROR");
        }
      }
    } else {
      this.setState({ stockStatus: "Out of Stock !" });
      toast.error("Out of Stock !");
    }
  };

  loadProductPreviewImages(product) {
    if (product.imageSize.length === 0) {
      return <img src={`${process.env.PUBLIC_URL}/assets/images/no-preview-image.png`} alt="" />;
    } else {
      let items = product.imageSize.slice(0, 2).map((image, index) => {
        if (index === 0) {
          return <img src={`${image.medium}`} alt="" key={index} />;
        } else {
          return <img src={`${image.medium}`} alt="" className="img-top" key={index} />;
        }
      });
      return items;
    }
  }

  reloadRoute(productid){
    window.location.href = `${process.env.PUBLIC_URL}/product/${productid}`;
  }

  render() {
    const { product, symbol, onAddToCartClicked, onAddToWishlistClicked, addToCartClicked, BuynowClicked, addToWishlistClicked, onAddToCompareClicked, language } = this.props;
    const { uniqueSize, uniqueColors, filteredCurrentStock ,secondImage, currentSalePrice,currentDiscount} = this.state;

    let RatingStars = [];
    for (var i = 0; i < product.rating; i++) {
      RatingStars.push(<i className="fa fa-star" key={i}></i>);
    }


    var productsnav = {
      vertical: this.state.vertical,
      verticalSwiping: this.state.vertical,
      slidesToShow: 6,
      slidesToScroll: 1,
      asNavFor: ".product-right-slick",
      arrows: false,
      infinite: true,
      centerMode: false,
      dots: false,
      focusOnSelect: true,
      responsive: [
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 6,
            slidesToScroll: 1
          }
        }
      ]
    };
 // console.log(this.props.itemCategory);
    return (
      <div className="product-box">
        <div className="plus-point-tab-item">
          <div className="img-wrapper">
            <div className="lable-block">
              {product.new == true ? <span className="lable3">{language === "en" ? "new" : "নতুন"}</span> : ""}
              {product.sale == true ? <span className="lable4">{language === "en" ? "on sale" : "বিক্রিতে"}</span> : ""}
            </div>
            <div className="imageBox">
            <a className="quick-view" href="javascript:void(0)" data-toggle="modal"
                               data-target="#quick-view"
                               title="Quick View"
                               onClick={this.onOpenModal}><i className="fa fa-eye" aria-hidden="true"></i></a>  
              <a style={{cursor: "pointer"}} onClick={e=>{this.reloadRoute(`${product.id}`)}}>
                <img
                  src={ this.state.secondImage.length < 1 ?`${ product.pictures[0] ? product.pictures[0] : `${process.env.PUBLIC_URL}/assets/images/no-preview-image.png`}` : this.state.secondImage }
                  className="img-fluid"
                  alt=""
                />
              </a>
            </div>
          </div>
          <div className="product-detail">
            <div>
              <p style={{ marginBottom: "5px", marginTop: "10px" }}>
                {language == "en" ? product.subDepartmentName : product.subDepartmentName_bd}
              </p>
              <Link to={`${process.env.PUBLIC_URL}/product/${product.id}`}>
                <h6 className="product-item-title">{language == "en" ? product.name : product.name_bd}</h6>
              </Link>
              <span className="old-price">
                <del>৳{currentSalePrice?currentSalePrice:product.sell_price}</del>
              </span>{" "}
              <span className="new-price">৳{currentSalePrice?currentSalePrice - (currentSalePrice* currentDiscount) / 100:product.sell_price - (product.sell_price * product.discount) / 100}</span>
              <div className="product-right">
                <div>
                  <div className="size-box">
                    <ul>
                      {uniqueSize.map((s, i) => {
                        if (s.toString() == this.state.size) {
                          return (
                              <li className="front-check-single-active"
                                  value={s.toString()}
                                  onClick={e => {
                                    this.setState({ size: e.currentTarget.getAttribute("value") }, this.calculateCurrentStock);
                                  }}
                                  key={i}
                                  id={i}
                              >
                                {s}
                              </li>
                          );
                        } else {
                          return (
                              <li className="front-check-single"
                                  value={s.toString()}
                                  onClick={e => {
                                    this.setState({ size: e.currentTarget.getAttribute("value") }, this.calculateCurrentStock);
                                  }}
                                  key={i}
                                  id={i}
                              >
                                {s}
                              </li>
                          );
                        }
                      })}
                    </ul>
                  </div>
                </div>
                <div>
                  <div className="size-box">
                    <ul>
                      {uniqueColors.map(color => {
                        if (color.length > 1) {
                          if (color == this.state.color) {
                            return (
                                <li
                                    value={color.toString()}
                                    onClick={e => {
                                      console.log(e.currentTarget.getAttribute("value"));
                                      this.setState({ color: e.currentTarget.getAttribute("value") }, this.calculateCurrentStock);
                                    }}
                                    style={{
                                      background: `${color}`,
                                      width: "25px",
                                      height: "25px",
                                      border: "3px solid #fff",
                                      boxShadow: "0 0 0 3px hsl(0, 0%, 80%)"
                                    }}
                                ></li>
                            );
                          } else {
                            return (
                                <li
                                    value={color.toString()}
                                    onClick={e => {
                                      console.log(e.currentTarget.getAttribute("value"));
                                      this.setState({ color: e.currentTarget.getAttribute("value") }, this.calculateCurrentStock);
                                    }}
                                    style={{ background: `${color}`, width: "25px", height: "25px", border: "1px solid #000" }}
                                ></li>
                            );
                          }
                        }
                      })}
                    </ul>
                  </div>
                </div>
              </div>
              <ul>
                <li>
                  <button className="item-add-to-cart-btn" onClick={this.handleAddToCart}>
                    {language === "en" ? "ADD TO CART" : "কার্ট যোগ করুন"}
                  </button>
                </li>
                <li>
                  <a href="#!" onClick={onAddToWishlistClicked}>
                    <img className="whish-icon" src={`${process.env.PUBLIC_URL}/assets/images/whish-icon.png`} alt="" />
                  </a>
                </li>
              </ul>
              <div className="rating">{RatingStars}</div>
            </div>
          </div>
          <Modal open={this.state.open} onClose={this.onCloseModal} center>
          <div className="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div className="modal-content quick-view-modal">
              <div className="modal-body">
                <div className="row">
                  
                <div className="col-lg-1 col-sm-2 col-xs-12 p-0">
                    <div className="row">
                      <div className="col-12 p-0">
                        <Slider
                          {...productsnav}
                          asNavFor={this.props.navOne}
                          ref={slider => (this.slider2 = slider)}
                          className="slider-nav"
                        >
                          {product.variants
                            ? product.variants.map((vari, index) => (
                                <div key={index}>
                                  <img src={`${vari.images}`} key={index} alt="" className="img-fluid" />
                                </div>
                              ))
                            : product.pictures.map((vari, index) => (
                                <div key={index}>
                                  <img
                                    onClick={() => {
                                      this.setState({ secondImage: product.imageSize[`${index}`].large });
                                    }}
                                    src={`${vari}`}
                                    key={index}
                                    alt=""
                                    className="img-fluid"
                                  />
                                </div>
                              ))}
                        </Slider>
                      </div>
                    </div>
                  </div>

                  <div className="col-lg-5 col-sm-10 col-xs-12  order-up">                    
                  {product && (
                      <ReactImageMagnify
                        {...{
                          smallImage: {
                            alt: "",
                            isFluidWidth: true,
                            src: this.state.secondImage.length < 1 ? product.pictures[0] : this.state.secondImage
                          },
                          largeImage: {
                            src: this.state.secondImage.length < 1 ? product.pictures[0] : this.state.secondImage,
                            width: 1300,
                            height: 1800
                          },
                          enlargedImagePosition: "over"
                        }}
                      />
                    )}
                  </div>                  
                  <div className="col-lg-6 rtl-text">
                    <div className="product-right">
                    <span style={{ background: "red", marginRight: "5px" }} className="in-stock-btn">
                      -{currentDiscount}%
                    </span>
                    <span className="in-stock-btn">{this.state.stockStatus}</span>
                      <h2> {product.name} </h2>
                      <span className="old-price">
                <del>৳{currentSalePrice?currentSalePrice:product.sell_price}</del>
              </span>{" "}
              <span className="new-price">৳{currentSalePrice?currentSalePrice - (currentSalePrice* currentDiscount) / 100:product.sell_price - (product.sell_price * product.discount) / 100}</span>
                      <div className="save-percentage">
                        {language == "en" ? "You Save:" : "আপনি সংরক্ষণ করুন:"}{" "}
                        <span>
                          {" "}
                          {symbol}
                          {currentSalePrice?(currentSalePrice* currentDiscount) / 100:(product.sell_price * product.discount) / 100}
                        </span>
                        <span> ({currentDiscount}% off)</span>
                      </div>
                      {product.variants ? (
                        <ul className="color-variant">
                          {product.variants.map((vari, i) => (
                            <li className={vari.color} key={i} title={vari.color} onClick={() => this.onClickHandle(vari.images)}></li>
                          ))}
                        </ul>
                      ) : (
                        ""
                      )}
                      <div className="border-product">
                        <h6 className="product-title">product details</h6>
                        <p>{product.shortDetails}</p>
                      </div>
                      <div className="product-description border-product">
                          <div className="size-box">
                            <ul>
                              {uniqueSize.map((s, i) => {
                                if (s.toString() == this.state.size) {
                                  return (
                                      <li className="front-check-single-active"
                                          value={s.toString()}
                                          onClick={e => {
                                            this.setState({ size: e.currentTarget.getAttribute("value") }, this.calculateCurrentStock);
                                          }}
                                          key={i}
                                          id={i}
                                      >
                                        {s}
                                      </li>
                                  );
                                } else {
                                  return (
                                      <li className="front-check-single"
                                          value={s.toString()}
                                          onClick={e => {
                                            this.setState({ size: e.currentTarget.getAttribute("value") }, this.calculateCurrentStock);
                                          }}
                                          key={i}
                                          id={i}
                                      >
                                        {s}
                                      </li>
                                  );
                                }
                              })}
                            </ul>
                          </div>
                        <div className="size-box">
                          <ul>
                            {uniqueColors.map(color => {
                              if (color.length > 1) {
                                if (color == this.state.color) {
                                  return (
                                      <li
                                          value={color.toString()}
                                          onClick={e => {
                                            console.log(e.currentTarget.getAttribute("value"));
                                            this.setState({ color: e.currentTarget.getAttribute("value") }, this.calculateCurrentStock);
                                          }}
                                          style={{
                                            background: `${color}`,
                                            width: "25px",
                                            height: "25px",
                                            border: "3px solid #fff",
                                            boxShadow: "0 0 0 3px hsl(0, 0%, 80%)"
                                          }}
                                      ></li>
                                  );
                                } else {
                                  return (
                                      <li
                                          value={color.toString()}
                                          onClick={e => {
                                            console.log(e.currentTarget.getAttribute("value"));
                                            this.setState({ color: e.currentTarget.getAttribute("value") }, this.calculateCurrentStock);
                                          }}
                                          style={{ background: `${color}`, width: "25px", height: "25px", border: "1px solid #000" }}
                                      ></li>
                                  );
                                }
                              }
                            })}
                          </ul>
                        </div>
                        <h6 className="product-title">quantity</h6>
                        <div className="qty-box">
                          <div className="input-group">
                              <span className="input-group-prepend">
                                <button type="button" className="btn quantity-left-minus" onClick={this.minusQty} data-type="minus" data-field="">
                                  <i className="fa fa-minus"></i>
                                </button>
                              </span>
                                          <input readOnly
                                                 type="text"
                                                 name="quantity"
                                                 value={this.state.quantity}
                                                 onChange={this.changeQty}
                                                 className="form-control input-number"
                                          />
                                          <span className="input-group-prepend">
                                <button type="button" className="btn quantity-right-plus" onClick={this.plusQty} data-type="plus" data-field="">
                                  <i className="fa fa-plus"></i>
                                </button>
                              </span>
                          </div>
                        </div>

                        <p style={{ margin: "10px 0" }}>
                          {language == "en" ? "Hurry! Only" : "তাড়াতাড়ি! কেবল"}{" "}
                          <span style={{ color: "red", fontWeight: "bold" }}>{filteredCurrentStock}</span>
                          {language == "en" ? " Left in Stock!" : "স্টক মধ্যে বাকি!"}
                        </p>

                        <div className="progress">
                          <div
                              className="progress-bar"
                              role="progressbar"
                              style={{ width: `${filteredCurrentStock}%` }}
                              aria-valuenow={product.stock}
                              aria-valuemin="0"
                              aria-valuemax="100"
                          ></div>
                        </div>

                        <p style={{ marginTop: "10px" }}>
                          {language == "en"
                              ? "Order in the next 24 Hours to get it by Tuesday 17/2/2020"
                              : "মঙ্গলবার 17/2/2020 এর মধ্যে পরবর্তী 24 ঘন্টা অর্ডার করুন"}
                        </p>

                      </div>
                      <div className="product-buttons">
                        <div className="row">
                          <div className="col-md-12">
                            <a className="btn btn-solid btn-cart " onClick={this.handleAddToCart}>
                              {language == "en" ? "add to cart" : "কার্ট যোগ করুন"}
                            </a>
                          </div>
                          <div className="col-md-6">
                            <a className="btn btn-solid btn-wishlist" onClick={() => addToWishlistClicked(product)}>
                              {language == "en" ? "add to wishlist" : "চাহিদাপত্রে যোগ করা"}
                            </a>
                          </div>
                          <div className="col-md-6">
                            <a className="btn btn-solid btn-compare" onClick={() => addToCartClicked(product, this.state.quantity)}>
                              {language == "en" ? "add to compare" : "তুলনা যোগ করুন"}
                            </a>
                          </div>
                          <div className="col-md-12">
                            <Link
                              to={`${process.env.PUBLIC_URL}/checkout`}
                              className="btn btn-solid btn-buy"
                              onClick={() => BuynowClicked(product, this.state.quantity)}
                            >
                              {language == "en" ? "buy now" : "এখন কিনুন"}
                            </Link>
                          </div>
                          <div className="col-md-12">
                            <img className="img-fluid" src={`${process.env.PUBLIC_URL}/assets/images/payment-getwaye.png`} alt="user" />
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  language: state.Intl.locale,
  symbol: state.data.symbol,
  cartItems: state.cartList.cart
});

export default connect(mapStateToProps, { addToCart })(ProductItem);
