/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";
import { connect } from "react-redux";
// let BASE_URL = "http://unlockgiftcards.com";
// const API_URL = `${BASE_URL}/api/v1/e-commerce/shop/products-new-arival`;

class findusPage extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { language } = this.props;
    return (
      <div>
        <div className="container" style={{ marginBottom: "40px", marginTop: "30px" }}>
          <div className="title4">
            <h4 className="text-xl-center">
              <span style={{ marginTop: "10px", padding: "0 10px 0 10px" }}>{language == "en" ? "Our Outlets" : "আমাদের আউটলেটস"}</span>
            </h4>
          </div>
        </div>
        <div className="container" >
          <div className="row">
            <div className="col-md-6"style={{ textAlign: "center", margin: "auto" }}>
            <h3><strong className="">Jamuna Future Park</strong></h3>
              <p>Baridhara, Kuril, Vatara</p>
              <p>Dhaka - 1205</p>
            </div>
            <div className="col-md-6">
              <div className="new-arrivals">
                <img
                  style={{ width: "100%" }}
                  className="img-fluid"
                  src={`${process.env.PUBLIC_URL}/assets/images/plus-point-shop.jpg`}
                  alt="new arrivals"
                />
              </div>
            </div>
          </div>
        </div>
        <div className="container" style={{ marginBottom: "40px", marginTop: "30px" }}>
          <div className="title4">
            <h4 className="text-xl-center">
              <span style={{ padding: "0 10px 0 10px" }}>{language == "en" ? "Other Outlets" : "অন্যান্য আউটলেট"}</span>
            </h4>
          </div>
        </div>
        <div className="container" >
          <div className="row">
            <div className="col-md-2"></div>
            <div className="col-md-4"style={{ textAlign: "center", margin: "auto" }}>
              <h4>Jamuna Future Park</h4>
              <p>Baridhara, Kuril, Vatara</p>
              <p>Dhaka - 1205</p>
            </div>
            <div className="col-md-4"style={{ textAlign: "center", margin: "auto" }}>
              <div className="new-arrivals">
                <img
                  style={{ width: "100%" }}
                  className="img-fluid"
                  src={`${process.env.PUBLIC_URL}/assets/images/plus-point-shop.jpg`}
                  alt="new arrivals"
                />
              </div>
            </div>
            <div className="col-md-2"></div>
          </div>
          <div className="row">
            <div className="col-md-2"></div>
            <div className="col-md-4"style={{ textAlign: "center", margin: "auto" }}>
              <h4>Jamuna Future Park</h4>
              <p>Baridhara, Kuril, Vatara</p>
              <p>Dhaka - 1205</p>
            </div>
            <div className="col-md-4">
              <div className="new-arrivals">
                <img
                  style={{ width: "100%" }}
                  className="img-fluid"
                  src={`${process.env.PUBLIC_URL}/assets/images/plus-point-shop.jpg`}
                  alt="new arrivals"
                />
              </div>
            </div>
            <div className="col-md-2"></div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  symbol: state.data.symbol,
  language: state.Intl.locale
});

export default connect(mapStateToProps, {})(findusPage);
