/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";
import { connect } from "react-redux";
import API from "../../../config"


class dhaka extends Component {
  constructor(props) {
    super(props);
    this.state={
      divisionData:[],
    }
  }

  componentDidMount() {
    this.fetchDivision();
  }
  fetchDivision() {
    fetch(API.outlet)
        .then(res => res.json())
        .then(myJson => {
          var divisionTop = myJson.data;
          let divison=this.props.match.params.divison;
          if (divison=='Barishal'){
            this.setState({
              divisionData: divisionTop.Barishal
            });
          }
          else if (divison=='Chattagram'){
            this.setState({
              divisionData: divisionTop.Chattagram
            });
          }
          else if (divison=='Rajshahi'){
            this.setState({
              divisionData: divisionTop.Rajshahi
            });
          }
          else if (divison=='Khulna'){
            this.setState({
              divisionData: divisionTop.Khulna
            });
          }
          else if (divison=='Barishal'){
            this.setState({
              divisionData: divisionTop.Barishal
            });
          }
          else if (divison=='Sylhet'){
            this.setState({
              divisionData: divisionTop.Sylhet
            });
          }
          else if (divison=='Dhaka'){
            this.setState({
              divisionData: divisionTop.Dhaka
            });
          }
          else if (divison=='Rangpur'){
            this.setState({
              divisionData: divisionTop.Rangpur
            });
          }
          else if (divison=='Mymensingh'){
            this.setState({
              divisionData: divisionTop.Mymensingh
            });
          }
        });
  }


  render() {
    const { language } = this.props;
    const { divisionData } = this.state;
    let divison=this.props.match.params.divison;

   let iframe='';
    if (divison=='Barishal'){
      iframe='https://maps.google.com/maps?q=plus%20point%20barisal&t=&z=13&ie=UTF8&iwloc=&output=embed'
    }
    else if (divison=='Chattagram'){
      iframe='https://maps.google.com/maps?q=plus%20point%20chittagong&t=&z=13&ie=UTF8&iwloc=&output=embed'
    }
    else if (divison=='Rajshahi'){
      iframe='https://maps.google.com/maps?q=plus%20point%20Rajshahi&t=&z=13&ie=UTF8&iwloc=&output=embed'
    }
    else if (divison=='Khulna'){
      iframe=''
    }
    else if (divison=='Barishal'){
      iframe='https://maps.google.com/maps?q=plus%20point%20barisal&t=&z=13&ie=UTF8&iwloc=&output=embed'
    }
    else if (divison=='Sylhet'){
      iframe='https://maps.google.com/maps?q=plus%20point%20Sylhet&t=&z=13&ie=UTF8&iwloc=&output=embed'
    }
    else if (divison=='Dhaka'){
      iframe='https://maps.google.com/maps?q=plus%20point%20Dhaka&t=&z=13&ie=UTF8&iwloc=&output=embed'
    }
    else if (divison=='Rangpur'){
      iframe='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3599.9642170233383!2d89.28037741545323!3d25.53956882399128!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39fcd13da1090335%3A0x6a46a7344af43208!2sPlus%20Point!5e0!3m2!1sen!2sbd!4v1581077776358!5m2!1sen!2sbd'
    }
    else if (divison=='Mymensingh'){
      iframe='https://maps.google.com/maps?q=plus%20point%20Mymensingh&t=&z=13&ie=UTF8&iwloc=&output=embed'
    }

    return (
      <div>
        <div className="container" style={{ marginBottom: "40px", marginTop: "30px" }}>
          <div className="title4">
            <h4 className="text-xl-center">
              <span style={{ marginTop: "10px", padding: "0 10px 0 10px" }}>{language == "en" ? `Our Outlets - ${divison}` : "আমাদের আউটলেটস"}</span>
            </h4>
          </div>
        </div>

        <div className="container" >
          <div className="row">
            <div className="col-md-6 map">            
              <div className="mapouter">
                    <div className="gmap_canvas">
                    <iframe
                        style={{ height: "393px" }}
                        width="100%"
                        id="gmap_canvas"
                        src={`${iframe}`}
                        frameborder="0"
                        scrolling="no"
                        marginheight="0"
                        marginwidth="0"
                    ></iframe>
                    </div>
                </div>
            </div>
            <div className="col-md-6">
              <div className="new-arrivals">
                {divisionData && divisionData.slice(0,1).map((divisionDataItem, index) => (
                <img
                  style={{ width: "100%" }}
                  className="img-fluid"
                  src={`${divisionDataItem.image}`}
                  alt="new arrivals"
                />
                 ))}
              </div>
            </div>
          </div>
        </div>

        <div className="container" style={{ marginBottom: "40px", marginTop: "30px" }}>
          <div className="title4">
            <h4 className="text-xl-center">
              <span style={{ padding: "0 10px 0 10px" }}>{language == "en" ? "Other Outlets" : "অন্যান্য আউটলেট"}</span>
            </h4>
          </div>
        </div>
        <div className="container" >
          {divisionData && divisionData.slice(1,40).map((divisionDataItem, index) => (
              <div>
            <div className="row">
                    <div className="col-md-2"></div>
                    <div className="col-md-4" style={{marginBottom: "30px", alignSelf: "center", textAlign: "center"}}>
                      <div className="utlet-text">
                        <h4>{`${divisionDataItem.name}`}</h4>
                        <p style={{ lineHeight: "20px" }}>{`${divisionDataItem.addressline}`}<br></br>
                        Mobile No : {`${divisionDataItem.phone}`}<br></br>
                        Email  : {`${divisionDataItem.email}`}<br></br>
                       </p>
                      </div>
                    </div>

                    <div className="col-md-4"style={{ textAlign: "center", marginBottom: "30px"}}>
                      <div className="new-arrivals" style={{marginBottom: "0"}}>
                        <img
                          style={{ width: "100%",height:"250px" }}
                          className="img-fluid"
                          src={`${divisionDataItem.image}`}
                          alt="new arrivals"
                        />
                      </div>
                    </div>
                  </div>
          </div>
          ))}
          
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  symbol: state.data.symbol,
  language: state.Intl.locale
});

export default connect(mapStateToProps, {})(dhaka);
