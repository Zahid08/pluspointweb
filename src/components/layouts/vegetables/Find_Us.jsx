/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import division from "../../../constants/division.json";

class Find_Us extends Component {
  constructor(props) {
    super(props);
    this.state = {     
      division,
      newDivision: ""
    };
  }

  getchangeDivision = event => {
    this.setState({ newDivision: event.target.value });
  };

  // Username(event) {
  //   this.setState({ Username: event.target.value });
  // }

  
  render() {
    const { language } = this.props; 
    const divi = this.state.newDivision;
    return (
      <div>
        <div className="container">
          <div className="title4">
            <h4 className="text-xl-center">
              <span>{language == "en" ? "Find Us Near You" : "আপনার কাছাকাছি আমাদের সন্ধান করুন"}</span>
            </h4>
          </div> 
        </div>
        <div className="container">
          <div className="row">
            <div className="col-md-6">
              <div className="new-arrivals find-us">
                <img
                  style={{ width: "100%" }}
                  className="img-fluid"
                  src={`${process.env.PUBLIC_URL}/assets/images/mobile-map.png`}
                  alt="new arrivals"
                />
              </div>
            </div>
            <div className="col-md-6" style={{ textAlign: "center", margin: "auto" }}>
                <div style={{display: "inline-block"}}>                
                  <select onChange={this.getchangeDivision} style={{height: "37px", border: "2px solid #000", fontWeight: "bold", verticalAlign: "middle"}}>
                    <option value="">Select Location</option>
                    {
                      this.state.division.map((value) =>
                      <option value={value.name}>{value.name}</option>
                      )
                    } 
                  </select>                  
                </div>
                <div className="btn btn-solid" style={{ padding: "6px", background: "#000 !important", color: "#fff !important", marginLeft:"5px"}} >
                  <Link to={`${process.env.PUBLIC_URL}/outlet/${divi}`} className="view-cart">
                    {language === "en" ? "Get Us" : "আমাদের পেতে"}
                  </Link>
                </div>      
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  symbol: state.data.symbol,
  language: state.Intl.locale
});

export default connect(mapStateToProps, {})(Find_Us);

