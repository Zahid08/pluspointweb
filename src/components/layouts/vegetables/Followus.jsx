/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";
import { connect } from "react-redux";
import API from "../../../config";
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

class Followus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      facebookItems: []
    };
  }

  componentDidMount() {
    this.getContent();
  }

  getContent() {
    fetch(API.products)
      .then(res => res.json())
      .then(json => {
        this.setState({
          items: json.data
        });
      });
    fetch(API.facebook)
      .then(res => res.json())
      .then(json => {
        this.setState({
          facebookItems: json.data
        });
      });
  }

  render() {
    var { facebookItems } = this.state;
    const { language } = this.props;
    return (
      <div>

        {/* Facebook  */}
        {/* <section className="section-b-space addtocart_count ratio_square"> */}
        <div className="container">
          <div className="title4">
            <h4 className="text-xl-center">
              <span>{language == "en" ? "Bhumisuta Fashion Follow Us on Facebook" : "ফেসবুকে আমাদের অনুসরণ করুন"}</span>
            </h4>
          </div>
          <br />
          <div className="container facebook">
            <div className="row">
            <OwlCarousel
                className="owl-theme"
                loop true
                margin={10}
                nav
                autoplay true
                items = {6}
                responsiveClass true
              >
              
              {facebookItems.slice(0, 100).map(item => {
                return (
                  <React.Fragment>
                    {/* <div class="item">
                      <a href={item.permalink_url} className="col-lg-2 col-md-2 col-sm-4">
                        <div className="category-item">
                          <div className="content">
                            <div className="wrap">

                              <img height="198" width="198" className="img" src={item.full_picture} alt="Generic placeholder image" />
                            </div>
                          </div>
                        </div>
                      </a>
                    </div> */}
                    <a href={item.permalink_url} className="item col-sm-4">
                        <div className="category-item">
                          <div className="content">
                            <div className="wrap">

                              <img height="198" width="198" className="img" src={item.full_picture} alt="Generic placeholder image" />
                            </div>
                          </div>
                        </div>
                      </a>
                    </React.Fragment>
                );
              })}
           
            </OwlCarousel>
            </div>
          </div>
        </div>
        <br />
        {/* FACEBOOK end  */}
        {/* </section> */}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  symbol: state.data.symbol,
  language: state.Intl.locale
});

export default connect(mapStateToProps, {})(Followus);
