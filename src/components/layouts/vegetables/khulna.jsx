/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";
import { connect } from "react-redux";


class khulna extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { language } = this.props;
    return (
      <div>
        <div className="container" style={{ marginBottom: "40px", marginTop: "30px" }}>
          <div className="title4">
            <h4 className="text-xl-center">
              <span style={{ marginTop: "10px", padding: "0 10px 0 10px" }}>{language == "en" ? "No Outlets Here" : "আমাদের আউটলেটস"}</span>
            </h4>
          </div>
        </div>
        
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  symbol: state.data.symbol,
  language: state.Intl.locale
});

export default connect(mapStateToProps, {})(khulna);
