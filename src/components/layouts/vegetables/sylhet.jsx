/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";
import { connect } from "react-redux";


class sylhet extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { language } = this.props;
    return (
      <div>
        <div className="container" style={{ marginBottom: "40px", marginTop: "30px" }}>
          <div className="title4">
            <h4 className="text-xl-center">
              <span style={{ marginTop: "10px", padding: "0 10px 0 10px" }}>{language == "en" ? "Our Outlets - Sylhet" : "আমাদের আউটলেটস"}</span>
            </h4>
          </div>
        </div>
        <div className="container" >
          <div className="row">
          <div className="col-lg-6 map">
            <div className="mapouter">
                <div className="gmap_canvas">
                <iframe
                    style={{ height: "316px" }}
                    width="100%"
                    id="gmap_canvas"
                    src="https://maps.google.com/maps?q=plus%20point&t=&z=11&ie=UTF8&iwloc=&output=embed"
                    frameborder="0"
                    scrolling="no"
                    marginheight="0"
                    marginwidth="0"
                ></iframe>
                </div>
            </div>
            </div>
              <div className="col-md-6">
              <div className="new-arrivals">
                <img
                  style={{ width: "100%" }}
                  className="img-fluid"
                  src={`${process.env.PUBLIC_URL}/assets/images/plus-point-shop.jpg`}
                  alt="new arrivals"
                />
              </div>
            </div>
            <div className="col-md-6"style={{textAlign: "center"}}>
            <h3><strong className="">Samira Complex</strong></h3>
              <p>Samira Complex, Dhopa Dighir Uttar Par, Hotel Anurag Opposite Site,Jail Road Sylhet</p>
              <p>Mobile No : 01958 387 981</p>
            <h3><strong className="">Sahib Shopping Complex</strong></h3>
              <p>Sahib Shopping Complex,Shop No – 1,2,3. 1st Floor. East Zindabazar, Sylhet</p>
              <p>Mobile No : 01958 387 980</p>
            </div>
            
          </div>
        </div> 
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  symbol: state.data.symbol,
  language: state.Intl.locale
});

export default connect(mapStateToProps, {})(sylhet);
