import React, { Component } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { connect } from "react-redux";

import { getBestSeller, getMensWear, getWomensWear } from "../../../services/index";
import { addToCart, addToWishlist, addToCompare } from "../../../actions/index";
import ProductItem from "./product-item";
import { Link } from "react-router-dom";

class SearchProductListing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      departmentList: [],
      subDepartment: [],
      products: []
    };
  }
  componentDidMount() {
    this.fetchDepartment();
  }

  loadProductPreviewImages(product) {
    if (product.imageSize.length === 0) {
      return <img src={`${process.env.PUBLIC_URL}/assets/images/no-preview-image.png`} alt="" />;
    }
    product.imageSize.map(image => {
      return <img src={`${process.env.PUBLIC_URL}/assets/images/item-01.png`} alt="" />;
    });
  }

  fetchDepartment() {
    fetch('https://pluspointapi.unlockretail.com/api/web/v1/department?access-token=9e74904aa7581d08c1465f8fc842630c')
        .then(res => res.json())
        .then(myJson => {
          var departmentList = myJson.data.slice(0, 3);
          this.setState({
            departmentList: departmentList
          });
        });
  }


  render() {
    var { departmentList } = this.state;
    let { newProducts, featuredProducts, specialProducts, language } = this.props;
    const { symbol, addToCart, addToWishlist, addToCompare } = this.props;

    return (
      <div>        
        {/* main category start  */}
         
        {/* main category end  */}
        <br></br>
        <br></br>

        {/* tab contents */}

        <section className="section-b-space p-t-0">
          <div className="container text-center">
            <Tabs className="theme-tab">
              <TabList className="tabs tab-title">
                <Tab>  </Tab>
                <Tab> </Tab>
                <Tab> </Tab>
              </TabList>

              <TabPanel>
                <div className="row">
                  {newProducts.slice(0, 4).map((product, index) => (
                    <ProductItem
                      product={product}
                      symbol={symbol}
                      onAddToCompareClicked={() => addToCompare(product)}
                      onAddToWishlistClicked={() => addToWishlist(product)}
                      key={index}
                      search="search"
                    />
                  ))}
                </div>
              </TabPanel>
              <Link to="/products/all" className="btn btn-solid">View More</Link>
              {/* <TabPanel>
                <div className="row">
                  {specialProducts.map((product, index) => (
                    <ProductItem
                      product={product}
                      symbol={symbol}
                      onAddToCompareClicked={() => addToCompare(product)}
                      onAddToWishlistClicked={() => addToWishlist(product)}
                      key={index}
                    />
                  ))}
                </div>
              </TabPanel>
              <TabPanel>
                <div className="row">
                  {featuredProducts.map((product, index) => (
                    <ProductItem
                      product={product}
                      symbol={symbol}
                      onAddToCompareClicked={() => addToCompare(product)}
                      onAddToWishlistClicked={() => addToWishlist(product)}
                      key={index}
                    />
                  ))}
                </div>
              </TabPanel> */}
            </Tabs>
          </div>
        </section>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  newProducts: getBestSeller(state.data.search),
  featuredProducts: getMensWear(state.data.products),
  specialProducts: getWomensWear(state.data.products),
  symbol: state.data.symbol,
  language: state.Intl.locale
});

export default connect(mapStateToProps, {
  addToCart,
  addToWishlist,
  addToCompare
})(SearchProductListing);
