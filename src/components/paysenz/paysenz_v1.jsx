import React, {Component} from 'react';

const requestData = { 'grant_type': 'password', 'client_id': 16, 'client_secret': '2XeYHZSAorBmqowLmCOUXFWLTb21kGRlekEfCe46','username':'info@unlocklive.com','password':'123456','scope':'*' };
class Paysenz extends Component {
    constructor(){
        super();
        this.state = {
            paysenztoken : '',
            orderId:'',
            successUrl:'http://ecom2.unlockretail.com/paysenz-order',//`${process.env.PUBLIC_URL}/order-success`
            failUrl:'http://ecom2.unlockretail.com/fail-order',//`${process.env.PUBLIC_URL}/order-success`,
            cancelUrl:'http://ecom2.unlockretail.com/cancel-order',//`${process.env.PUBLIC_URL}/order-success`,
            ipnUrl:'http://ecom2.unlockretail.com/ipn-order',//`${process.env.PUBLIC_URL}/order-success`,
        }
    }
    componentWillMount(){
        this.setState({orderId:1 + (Math.random() * (100-1))})
        this.getRetriveToken(requestData);
    }
    getRetriveToken=(requestData)=>{
        fetch("http://sandbox.paysenz.com/oauth/token", {
            method: "post",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
            },
            body: JSON.stringify({
                'grant_type':'password',
                'client_id':'16',
                'client_secret':'2XeYHZSAorBmqowLmCOUXFWLTb21kGRlekEfCe46',
                'username':'info@unlocklive.com',
                'password':'123456',
                'scope':'*'
            })
        }).then(resp => resp.json())
            .then(data => {
                 this.setState({paysenztoken:data});
                //this.PassPaysenz(data.access_token,this.props.location.state);
            })
    }

    PassPaysenz(token,data) {
        console.log(data);
        if (data) {
            fetch("http://sandbox.paysenz.com/api/v1.0/pay", {
                method: "post",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'X-Requested-With': 'XMLHttpRequest',
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    'buyer_name': data.billing.first_name ? data.billing.first_name : '',
                    'buyer_email': data.billing.email ? data.billing.email : '',
                    'buyer_address': data.billing.address ? data.billing.address : '',
                    'cus_city': 'City',
                    'cus_state': 'State',
                    'cus_postcode': data.billing.pincode ? data.billing.pincode : '',
                    'cus_country': 'Bangladesh',
                    'buyer_contact_number': data.billing.phone ? data.billing.phone : '',
                    'client_id': requestData.client_id ? requestData.client_id : '',
                    'order_id_of_merchant': this.state.orderId ? 'Paysez' + this.state.orderId : '',
                    'amount': data.orderTotal ? String(data.orderTotal ): '',
                    'currency_of_transaction': 'BDT',
                    'callback_success_url': this.state.successUrl ? this.state.successUrl : '',
                    'callback_fail_url': this.state.failUrl ? this.state.failUrl : '',
                    'callback_cancel_url': this.state.cancelUrl ? this.state.cancelUrl : '',
                    'callback_ipn_url': this.state.ipnUrl ? this.state.ipnUrl : '',
                    'order_details': this.state.orderId ? 'Payment for ApplicationID:' + this.state.orderId : '',
                    'expected_response_type': 'JSON',
                    'custom_1': '',
                    'custom_2': '',
                    'custom_3': '',
                    'custom_4': '',
                })
            }).then(resp => resp.json())
                .then(response => {
                    if (response.expected_response) {
                        const url = response.expected_response;
                        window.location = url;
                    }
                    console.log(response);
                })
        }
    }
    render (){
        return (
                <div className="col-lg-12">
                    <h3 style={{textAlign: "Left",color:"2px solid #ff4c3b"}}>Your Order Is Processing..</h3>
                </div>
        )
    }
}

export default Paysenz