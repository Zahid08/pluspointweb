import React, { Component } from "react";
import { Link } from "react-router-dom";
import { IntlActions } from "react-redux-multilingual";
import Pace from "react-pace-progress";
import $ from "jquery";

// Import custom components
import store from "../../../store";
import NavBar from "./common/navbar";
import CartContainer from "./../../../containers/CartContainer";
import LogoImage from "./common/logo";
import { changeCurrency, changeUserType, getSearchFromProducts, flushSearchFromProducts } from "../../../actions";
import { connect } from "react-redux";
import SearchProductListing from "../../layouts/vegetables/search-products-listing";

class HeaderTwo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      token: "",
      username: "",
      isLoading: false,
      MenuItems: []
    };
  }

  /*=====================
           Pre loader
           ==========================*/
  componentDidMount() {
    setTimeout(function() {
      document.querySelector(".loader-wrapper").style = "display: none";
      // document.querySelector(".item-add-to-cart-btn").style = "display: none";
    }, 300);
    setTimeout(function() {
      // document.querySelector(".item-add-to-cart-btn").style = "display: none";
    },500);
    this.props.flushSearchFromProducts("000");
  }

  componentWillMount() {
    window.addEventListener("scroll", this.handleScroll);
    const tokenVar = localStorage.getItem("token");
    this.setState({
      token: tokenVar
    });

    const Username = localStorage.getItem("username");
    this.setState({
      username: Username
    });
  }
  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = () => {
    let number = window.pageXOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

    if (number >= 300) {
      if (window.innerWidth < 576) {
        document.getElementById("sticky").classList.remove("fixed");
      } else document.getElementById("sticky").classList.add("fixed");
    } else {
      document.getElementById("sticky").classList.remove("fixed");
    }
  };

  Logout = event => {
    this.props.changeUserType("Normal");
    localStorage.removeItem("token");
    localStorage.removeItem("username");
    window.location.href = "/login";
  };

  changeLanguage(lang) {
    store.dispatch(IntlActions.setLocale(lang));
  }

  openNav() {
    var openmyslide = document.getElementById("mySidenav");
    if (openmyslide) {
      openmyslide.classList.add("open-side");
    }
  }
  openSearch() {
    document.getElementById("search-overlay").style.display = "block";

  }

  resetSearch() {
    console.log(this.props.search,'hhhhhhhhhhhh');
    if(this.props.search.length){
      document.getElementById("exampleInputPassword1").value = "";
      this.props.flushSearchFromProducts("000");
    }
    else{
      document.getElementById("search-overlay").style.display = "none";
    }

  }
  closeSearch() {
    window.location.reload();
  }
  outSideSearch(e) {
    if($(e.target).attr("class") == undefined){
      document.getElementById("search-overlay").style.display = "none";
    }

  }
  getSearchFromProducts(e) {
    console.log(e.target.value,'sssss');
    this.props.getSearchFromProducts($.trim(e.target.value));
    this.setState({searchItems : e.target.value});
  }
  load = () => {
    this.setState({ isLoading: false });
    fetch().then(() => {
      // deal with data fetched
      this.setState({ isLoading: false });
    });
  };

  render() {
    const { translate, language, search } = this.props;
    console.log('FFFFFFFFFFFFFFFFFFFFFFFFF');
    console.log(search);
    const { token, username } = this.state; //User Login Token
    const { userType } = this.props;
    return (
        <div>
          <header id="sticky" className="sticky" style={{ borderBottom: "1px solid #ddd" }}>
            {this.state.isLoading ? <Pace color="#27ae60" /> : null}
            <div className="mobile-fix-option"></div>
            {/*Top Header Component*/}
            <div style={{ borderBottom: "1px solid #ddd" }}>
              <div className="container">
                <div className="row">
                  <div className="col-sm-12">
                    <div className="main-menu border-top-0">
                      <div className="brand-logo layout2-logo">
                        <LogoImage logo={this.props.logoName} />
                      </div>
                      <div className="menu-right pull-right">
                        <div>
                          <div className="icon-nav">
                            <ul>
                              {token ? (
                                  <li>
                                    <div className="dropdown">
                                      <button
                                          style={{ background: "none" }}
                                          className="btn dropdown-toggle bdt-button-custom"
                                          type="button"
                                          id="dropdownMenuButton"
                                          data-toggle="dropdown"
                                          aria-haspopup="true"
                                          aria-expanded="false"
                                      >
                                        <img src={`${process.env.PUBLIC_URL}/assets/images/user-icon.png`} alt="user" />{" "}
                                        <span className="top-user-name">{username}</span>
                                        <img src={`${process.env.PUBLIC_URL}/assets/images/button-drop-arrow.png`} />
                                      </button>
                                      {userType != "Guest" ? (
                                          <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <ul className="onhover-show-div">
                                              <li>
                                                <Link to={`${process.env.PUBLIC_URL}/dashboard`} data-lng="en">
                                                  {language == "en" ? "Dashboard" : "ড্যাশবোর্ড"}
                                                </Link>
                                              </li>
                                              <li>
                                                <Link to={`${process.env.PUBLIC_URL}/my-orders`} data-lng="en">
                                                  {language == "en" ? "Your Orders" : "আপনার অর্ডার"}
                                                </Link>
                                              </li>
                                              <li>
                                                <button data-lng="en" onClick={this.Logout}>
                                                  {language == "en" ? "LogOut" : "লগআউট"}
                                                </button>
                                              </li>
                                            </ul>
                                          </div>
                                      ) : (
                                          <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <ul className="onhover-show-div">
                                              <li>
                                                <button data-lng="en" onClick={this.Logout}>
                                                  {language == "en" ? "LogOut" : "লগআউট"}
                                                </button>
                                              </li>
                                            </ul>
                                          </div>
                                      )}
                                    </div>
                                  </li>
                              ) : (
                                  <li>
                                    <Link to={`${process.env.PUBLIC_URL}/login`} data-lng="en">
                                      <img src={`${process.env.PUBLIC_URL}/assets/images/user-icon.png`} alt="user" />{" "}
                                      {language == "en" ? "Sign In" : "সাইন ইন করুন"}
                                    </Link>
                                  </li>
                              )}

                              {/*Header Cart Component */}

                              <li className="mobile-wishlist">
                                <Link to={`${process.env.PUBLIC_URL}/wishlist-checkout`}>
                                  <div>
                                    <img src={`${process.env.PUBLIC_URL}/assets/images/whish-list-icon.png`} className="img-fluid" alt="" />
                                    <i></i>
                                  </div>
                                </Link>
                              </li>
                              <li>
                                <CartContainer />
                              </li>
                              <li>
                                <div className="dropdown">
                                  <button
                                      style={{ background: "none" }}
                                      className="btn dropdown-toggle bdt-button-custom"
                                      // type="button"
                                      // id="dropdownMenuButton"
                                      // data-toggle="dropdown"
                                      // aria-haspopup="true"
                                      // aria-expanded="false"
                                  >
                                    {language == "en" ? "BDT (TK)" : "বিডিটি (টাকা)"}{" "}
                                    {/* <img src={`${process.env.PUBLIC_URL}/assets/images/button-drop-arrow.png`} /> */}
                                  </button>
                                  {/* <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                  <a className="dropdown-item" href="#">
                                    {language == "en" ? "US Dollar" : "আমেরিকান ডলার"}
                                  </a>
                                </div> */}
                                </div>
                              </li>
                              {/*language setting area*/}
                              <li className="mobile-setting">
                                <div className="dropdown">
                                  <button
                                      style={{ background: "none" }}
                                      className="btn dropdown-toggle bdt-button-custom"
                                      type="button"
                                      id="dropdownMenuButton"
                                      data-toggle="dropdown"
                                      aria-haspopup="true"
                                      aria-expanded="false"
                                  >
                                    {language == "en" ? "English" : "বাংলা"}{" "}
                                    <img src={`${process.env.PUBLIC_URL}/assets/images/button-drop-arrow.png`} />
                                  </button>
                                  <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <ul>
                                      <li>
                                        <button href={null} onClick={() => this.changeLanguage("en")}>
                                          {language == "bn" ? "ইংরেজি" : "English"}
                                        </button>{" "}
                                      </li>
                                      <li>
                                        <button href={null} onClick={() => this.changeLanguage("bn")}>
                                          {language == "bn" ? "বাংলা" : "Bangla"}
                                        </button>{" "}
                                      </li>
                                    </ul>
                                  </div>
                                </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="container">
              <div className="row">
                <div className="col-lg-11 col-sm-10 col-md-10">
                  <div className="main-nav-center">
                    <NavBar />
                  </div>
                </div>
                <div className="col-lg-1 col-sm-2 col-md-2" style={{ margin: "auto 0", textAlign: "right" }}>
                  <i className="fa fa-search main-search" onClick={this.openSearch}></i>
                </div>
              </div>
            </div>
            <div style={{ borderBottom: "1px solid #ddd" }}></div>
          </header>

          <div id="search-overlay" className="search-overlay" onClick={this.outSideSearch.bind(this)}>
            <div>
              <div className="overlay-content" >
                <div className="container">
                  <div className="row">
                    <div className="col-xl-12">
                      <form>
                        <div className="form-group">
                        <span onClick={this.closeSearch.bind(this)} style={{cursor: "pointer", padding: "10px", position: "absolute", top: "7px", fontSize: "15pt", color: "#777777"}}>
                          <i className="fa fa-search"></i>
                        </span>
                          <input type="text" autoComplete="off" onKeyUp={this.getSearchFromProducts.bind(this)} className="form-control" style={{paddingLeft: "35px"}} id="exampleInputPassword1" placeholder="Search a Product" />
                          <span className="closebtn" onClick={this.resetSearch.bind(this)} title="Close Overlay" style={{right:"25px", fontSize:"35px", top:"10px"}}>x</span>

                        </div>
                      </form>
                      { search && search.length > 0 && (<div className="col-xl-12">
                        <SearchProductListing category="" colSize="3"/>
                      </div>)}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    );
  }
}

const mapStateToProps = state => ({
  language: state.Intl.locale,
  userType: state.userType.userType,
  search: state.data.search
});

export default connect(mapStateToProps, { changeCurrency, changeUserType, getSearchFromProducts, flushSearchFromProducts})(HeaderTwo);
