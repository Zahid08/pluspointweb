import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import departments_json from "../../../../constants/departments.json";
import { withTranslate } from 'react-redux-multilingual'
import { getMatchProduct } from "../../../../services";
import ImageZoom from "../../../products/common/product/image-zoom";
import API from "../../../../config";

class NavBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      navClose: { right: "0px" },
      MenuItems: []
    };
    this.fetchMenuItems();
  }

  componentDidMount() {
    if (window.innerWidth < 750) {
      this.setState({ navClose: { right: "-410px" } });
    }
    if (window.innerWidth < 1199) {
      this.setState({ navClose: { right: "-300px" } });
    }
  }

  openNav() {
    console.log("open");
    this.setState({ navClose: { right: "0px" } });
  }
  closeNav() {
    this.setState({ navClose: { right: "-410px" } });
  }

  onMouseEnterHandler() {
    if (window.innerWidth > 1199) {
      document.querySelector("#main-menu").classList.add("hover-unset");
    }
  }

  handleSubmenu = event => {
    if (event.target.classList.contains("sub-arrow")) return;
    if (event.target.nextElementSibling.classList.contains("opensubmenu")) event.target.nextElementSibling.classList.remove("opensubmenu");
    else {
      document.querySelectorAll(".nav-submenu").forEach(function (value) {
        value.classList.remove("opensubmenu");
      });
      event.target.nextElementSibling.classList.add("opensubmenu");
    }
  };

  handleMegaSubmenu = event => {
    if (event.target.classList.contains("snullub-arrow")) return;

    if (event.target.parentNode.nextElementSibling.classList.contains("opensubmegamenu"))
      event.target.parentNode.nextElementSibling.classList.remove("opensubmegamenu");
    else {
      document.querySelectorAll(".menu-content").forEach(function (value) {
        value.classList.remove("opensubmegamenu");
      });
      event.target.parentNode.nextElementSibling.classList.add("opensubmegamenu");
    }
  };

  fetchMenuItems() {
    fetch(API.department, {
      method: "GET",
      cache: "no-cache"
    }).then(res => res.json())
      .then(myJson => {
        var departmentList = myJson.data.slice(0, 10);
        this.setState({
          MenuItems: departmentList
        });
      });
  }
  loadMenu1(menuslug) {
    if (menuslug) {
      let productItems;
      productItems = this.props.getProduct.filter(product => {
        if (product.departmentSlug == menuslug) {
          return product;
        }
      });
      if (productItems.length > 0) {
        return <div>
          {
            productItems.slice(0, 1).map((data, index) =>
              <div key={index}>
                <Link to={`${process.env.PUBLIC_URL}/product/${data.id}`}>
                  <img src={`${data.pictures[0]}`} alt="" />
                </Link>
                <p><b>NEW IN</b></p>
                <p>Spring/Summer 2020 Collection</p>
              </div>
            )
          }
        </div>
      } else {
        return <div>
          <img src={`${process.env.PUBLIC_URL}/assets/images/no-preview-image.png`} alt="" />
          <p><b>NEW IN</b></p>
          <p>Spring/Summer 2019 Collection</p>
        </div>
      }
    }

  }
  loadMenu2(menuslug) {
    if (menuslug) {
      let productItems;
      productItems = this.props.getProduct.filter(product => {
        if (product.departmentSlug == menuslug) {
          return product;
        }
      });
      if (productItems.length > 0) {
        return <div className="menu-content text-center">
          {
            productItems.slice(1, 2).map((data, index) =>
              <div key={index}>
                <Link to={`${process.env.PUBLIC_URL}/product/${data.id}`}>
                  <img src={`${data.pictures[0]}`} alt="" />
                </Link>
                <p><b>NEW IN</b></p>
                <p>Spring/Summer 2020 Collection</p>
              </div>
            )
          }
        </div>
      }
      else {
        return <div>
          <img src={`${process.env.PUBLIC_URL}/assets/images/no-preview-image.png`} alt="" />
          <p><b>NEW IN</b></p>
          <p>Spring/Summer 2019 Collection</p>
        </div>
      }

    }

  }

  render() {
    const { language, getProduct } = this.props;

    let top_product_items;
    top_product_items = getProduct.filter(product => {
      if (product.departmentSlug == 'top') {
        return product;
      }
    });
    let bootom_product_items;
    bootom_product_items = getProduct.filter(product => {
      if (product.departmentSlug == 'bottom') {
        return product;
      }
    });

    let footer_product_items;
    footer_product_items = getProduct.filter(product => {
      if (product.departmentSlug == 'footwear') {
        return product;
      }
    });

    const { MenuItems } = this.state;

    return (
      <div>
        <div className="main-navbar">
          <div id="mainnav">
            <div className="toggle-nav" onClick={this.openNav.bind(this)}>
              <i className="fa fa-bars sidebar-bar"></i>
            </div>
            <ul className="nav-menu" style={this.state.navClose}>
              <li className="back-btn" onClick={this.closeNav.bind(this)}>
                <div className="mobile-back text-right">
                  <span>Back</span>
                  <i className="fa fa-angle-right pl-2" aria-hidden="true"></i>
                </div>
              </li>
                {/* Start mega menu */}
                {MenuItems.map((Menu, index) => (
                  <li className="mobile-menu">
                    <a href={`${process.env.PUBLIC_URL}/products/${Menu.department_slug}`} className="dropdown" onClick={(e) => this.handleSubmenu(e)}>
                      {language === "en" ? Menu.department_name : Menu.department_name_bd}
                    </a>
                    {Menu.subdepartment.slice(0, 30).map((subdepartment, index) => (
                      <div>
                        <div className="link-section">
                          <div className="menu-title" >
                            <a href={`${process.env.PUBLIC_URL}/products/${Menu.department_slug}/${subdepartment.subdepartment_slug}`} >
                              <h5>
                                {language === "en" ? subdepartment.subdepartment_name : subdepartment.subdepartment_name_bd}
                                {/* <span className="sub-arrow"></span> */}
                              </h5>
                            </a>
                          </div>
                          <div className="menu-content" >
                            <ul>
                              {subdepartment.subcategory.slice(0, 20).map((subcategory, index) => (
                                <li className="up-text"><a href={`${process.env.PUBLIC_URL}/products/${Menu.department_slug}/${subdepartment.subdepartment_slug}/${subcategory.subcategory_name_slug}`} >{language === "en" ? subcategory.subcategory_name : subcategory.subcategory_name_bd}</a></li>
                              ))}
                            </ul>
                          </div>
                        </div>
                      </div>
                    ))}
                  </li>
                ))}
                {/* end mega menu */}
              {/* Start mega menu */}
              {MenuItems.map((Menu, index) => (
                <li className="mega-menu">
                  <a href={`${process.env.PUBLIC_URL}/products/${Menu.department_slug}`} className="dropdown" onClick={(e) => this.handleSubmenu(e)}>
                    {language === "en" ? Menu.department_name : Menu.department_name_bd}
                    <span className="sub-arrow"></span>
                  </a>
                  <div className="mega-menu-container" >
                    <div className="container">
                      <div className="row">
                        <div className="col-3 mega-box">
                          <div className="link-section">
                            <div className="menu-content text-center">
                              {this.loadMenu1(Menu.department_slug)}
                            </div>
                          </div>
                        </div>
                        <div className="col-6">
                          <div className="row">
                            {Menu.subdepartment.slice(0, 30).map((subdepartment, index) => (
                              <div className="col-4 mega-box">
                                <div className="link-section">
                                  <div className="menu-title" >
                                    <a href={`${process.env.PUBLIC_URL}/products/${Menu.department_slug}/${subdepartment.subdepartment_slug}`} >
                                      <h5>
                                        {language === "en" ? subdepartment.subdepartment_name : subdepartment.subdepartment_name_bd}
                                        <span className="sub-arrow"></span>
                                      </h5>
                                    </a>
                                  </div>
                                  <div className="menu-content" >
                                    <ul>
                                      {subdepartment.subcategory.slice(0, 20).map((subcategory, index) => (
                                        <li className="up-text"><a href={`${process.env.PUBLIC_URL}/products/${Menu.department_slug}/${subdepartment.subdepartment_slug}/${subcategory.subcategory_name_slug}`} >{language === "en" ? subcategory.subcategory_name : subcategory.subcategory_name_bd}</a></li>
                                      ))}
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            ))}
                          </div>
                        </div>

                        <div className="col-3 mega-box">
                          <div className="link-section">
                            {this.loadMenu2(Menu.department_slug)}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              ))}
              {/* end mega menu */}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  language: state.Intl.locale,
  getProduct: state.data.products,
});

export default connect(mapStateToProps)(NavBar);