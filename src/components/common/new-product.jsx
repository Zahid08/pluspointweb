import React, { Component } from "react";
import Slider from "react-slick";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { getBestSeller } from "../../services";

class NewProduct extends Component {
  render() {
    const { items, language } = this.props;

    var arrays = [];
    while (items.length > 0) {
      arrays.push(items.splice(0, 3));
    }

    return (
      <div className="theme-card">
        <h5 className="title-border">{language === "en" ? "new product" : "নতুন পণ্য"}</h5>
        <Slider className="offer-slider slide-1">
          {arrays.map((products, index) => (
            <div key={index}>
              {products.map((product, i) => (
                <div className="media" key={i}>
                  <Link to={`${process.env.PUBLIC_URL}/product/${product.id}`}>
                    <img className="img-fluid" src={`${product.pictures[0]? product.pictures[0] : `${process.env.PUBLIC_URL}/assets/images/no-preview-image.png` }`} alt="" />
                  </Link>
                  <div className="media-body media-body-custom align-self-center">
                    <p style={{ marginBottom: "5px", marginTop: "10px" }}>
                      {language === "en" ? product.subDepartmentName : product.subDepartmentName_bd}
                    </p>
                    <Link to={`${process.env.PUBLIC_URL}/product/${product.id}`}>
                      <h6 className="product-item-title">{language === "en" ? product.name : product.name_bd}</h6>
                    </Link>
                    <span className="old-price">
                      <del>৳{product.sell_price}</del>
                    </span>{" "}
                    <span className="new-price">৳ {product.sell_price - (product.sell_price * product.discount) / 100}</span>
                    <div className="rating">
                      <i className="fa fa-star"></i>
                      <i className="fa fa-star"></i>
                      <i className="fa fa-star"></i>
                      <i className="fa fa-star"></i>
                      <i className="fa fa-star"></i>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          ))}
        </Slider>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  items: getBestSeller(state.data.products, ownProps.category),
  symbol: state.data.symbol,
  language: state.Intl.locale
});

export default connect(mapStateToProps, null)(NewProduct);
