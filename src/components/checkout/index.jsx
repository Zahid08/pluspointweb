import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { Link, Redirect } from "react-router-dom";
import SimpleReactValidator from "simple-react-validator";
import _ from "lodash";
import axios from "axios";

import Breadcrumb from "../common/breadcrumb";
import { removeFromWishlist } from "../../actions";
import { getCartTotal, getDepartments, getTotalDiscount } from "../../services";
import Paysenz from "../paysenz/paysenz";
import district from "../../constants/district.json";
import division from "../../constants/division.json";
import upazilla from "../../constants/upazilla.json";
import themeSettings from "../common/theme-settings";
import API from "../../config"

class checkOut extends Component {
    constructor(props) {
        super(props);

        this.state = {
            payment: "stripe",
            first_name: "",
            last_name: "",
            phone: "",
            email: "",
            country: "Bangladesh",
            division: "",
            district: "",
            upazilla: "",
            branch: "",
            address: "",
            coupon: "",
            discountAmount:0,
            couponErrorMessage: "",
            colorCode:'red',
            city: "",
            state: "",
            pincode: "",
            branchInforamtion: [],
            create_account: "",
            showClickButoon: true,
            order_id: "",
            order_number: "",
            billing_address: [],
            user_id: "",
            customerInformation: [],
            customerId: "",
            orderId: 32423,
            registrationStatus: false,
            cashbackButtonStatus:'',
            paymentStatus:'',
            disableStatus:''
        };
        this.validator = new SimpleReactValidator();
        this.PaysenzClick = this.PaysenzClick.bind(this);
        this.getDiscountClick = this.getDiscountClick.bind(this);
    }

    async guestRegistration(event) {
        let Promise = await fetch(
            API.signup,
            {
                method: "post",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    "X-Requested-With": "XMLHttpRequest"
                },
                body: JSON.stringify({
                    userType: "3",
                    title: "Guest",
                    phone: "01618026798",
                    name: this.state.first_name + " " + this.state.last_name,
                    username: "_guest_" + Math.floor(Math.random() * 1000).toString(),
                    password: (new Date().getUTCMilliseconds() * 1000000).toString(),
                    email: this.state.email,
                    gender: "Guest"
                })
            }
        );
        let result = await Promise.json();
        console.log("result", result);
        if (result.data.status === 422) {
            const user = JSON.parse(result.data.message);
            console.log("Could not register", result.data);
            alert("Could not register");
            return false;
        } else {
            console.log(result.data);
            console.log("Success");
            this.setState({ registrationStatus: true }, () => {
                localStorage.setItem("username", result.data.username);
                localStorage.setItem("custId", result.data.custId);
                localStorage.setItem("author_id", result.data.id);
                localStorage.setItem("token", result.data.access_token);
                localStorage.setItem("msg_token", this.state.registration_success_message);
                console.log("guest registration done");
            });
            return true;
        }
    }

    onDivisionChangeHandler = event => {
        this.setState({ division: event.target.value });
        this.setState({ district: "", upazilla: "" });
       // this.getBranchInformation(event.target.value);
    };

    onDistrictChangeHandler = event => {
        this.setState({ district: event.target.value });
        this.setState({ upazilla: "" });
       // this.getBranchInformation(this.state.division,event.target.value);
    };

    //onBranchChangeHandler
    onBranchChangeHandler = event => {
        this.setState({ branch: event.target.value });
    };

    onUpazillaChangeHandler = event => {
        console.log(event.target);
        this.setState({ upazilla: event.target.value });
       // this.getBranchInformation(this.state.division,this.state.district,event.target.value);
    };

    getBranchInformation( division = "", distric = "", upazilla=""){
        fetch(`${API.root_url}/api/web/v1/branch/get-branch-by-id?access-token=9e74904aa7581d08c1465f8fc842630c&divisionId=${division}&districId=${distric}&upozelaId=${upazilla}`)
            .then(res => res.json())
            .then(myJson => this.setState({
                branchInforamtion: myJson.data
            }));
    }

    setStateFromCheckbox = event => {
        var obj = {};
        obj[event.target.name] = event.target.checked;
        this.setState(obj);

        if (!this.validator.fieldValid(event.target.name)) {
            this.validator.showMessages();
        }
    };

    checkhandle(value) {
        this.setState({
            payment: value
        });
    }

    async componentDidMount() {
        const customerId = localStorage.getItem("custId");
        this.setState({ customerId: customerId });
        // alert(customerId);
        if (customerId) {
            let reponse = await this.getCustomerInformationById(customerId);
        }
        const { customerInformation } = this.state;

        var customerNameSplit = String(customerInformation.name ? customerInformation.name : "").split(/[ ,]+/);
        this.setState({ first_name: customerNameSplit[0] });
        this.setState({ last_name: customerNameSplit[1] });
        this.setState({ email: customerInformation.email ? customerInformation.email : "" });
    }

    async getCustomerInformationById(customerId) {
        let response = await fetch(
            `${API.root_url}/api/web/v1/customers/${customerId}?access-token=${API.token}&expand=customer`,
            {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Accept: "application/json"
                }
            }
        );

        let data = await response.json();
        console.log("UUUUUUUU", data.data);
        this.setState({
            customerInformation: data.data
        });
    }

    CashOnDeliveryClick = () => {
        if (this.validator.allValid()) {
            this.setState({paymentStatus:'1'})

            if (this.props.userType == "Guest") {
                this.guestRegistration().then(data => {
                    if (data) {
                        this.SaveBillingOrder("CashOnDelivery", "Cash");
                        this.setState({cashbackButtonStatus:'1'})
                        setTimeout(() => {
                            if(this.state.order_id){
                                //alert("You submitted the form and stuff!");
                                localStorage.setItem("payment_method", 'cash');
                                this.props.history.push({
                                    pathname: `/order-success/${this.state.order_id}`,
                                    state: {
                                        payment: "payment30101",
                                        items: this.props.cartItems,
                                        orderTotal: this.props.total,
                                        symbol: this.props.symbol
                                    }
                                });
                            }
                        },1500);


                    }
                });
            } else {
                this.SaveBillingOrder("CashOnDelivery", "Cash");
                this.setState({cashbackButtonStatus:'1'})
                setTimeout(() => {
                    if(this.state.order_id){
                        // alert("You submitted the form and stuff!");
                        localStorage.setItem("payment_method", 'cash');
                        this.props.history.push({
                            pathname: `/order-success/${this.state.order_id}`,
                            state: {
                                payment: "payment30101",
                                items: this.props.cartItems,
                                orderTotal: this.props.total,
                                symbol: this.props.symbol
                            }
                        });
                    }
                },1500);
            }
        } else {
            this.validator.showMessages();
            // rerender to show messages for the first time
            this.forceUpdate();
        }
    };

    //paysenz button click and check validation
    PaysenzClick = () => {
        if (this.validator.allValid()) {
            this.setState({disableStatus:1});
            this.SaveBillingOrder("Paysenz", "OnlinePayment");
        } else {
            this.validator.showMessages();
            this.forceUpdate();
        }
    };

    // Start-----Coupon Checking Within Date Range And Sub Category Wise
    getDiscountClick = () => {
        var couponNumber=this.state.coupon;
        if (couponNumber) {
            fetch(`${API.root_url}/api/web/v1/coupon?access-token=9e74904aa7581d08c1465f8fc842630c&search=${couponNumber}`, {
                method: "GET",
                cache: "no-cache"
            }).then(res => res.json())
                .then(responseJson => {

                    //if response proper then set data
                    if (responseJson.status==200){
                        var currentDate = new Date().toJSON().slice(0,10).replace(/-/g,'-');
                        var couponStartDate=responseJson.data[0].couponStartDate;
                        var couponEndDate=responseJson.data[0].couponEndDate;
                        var subdeptId=responseJson.data[0].subdeptId;
                        var couponAmount=responseJson.data[0].couponAmount;

                        //Checking Date Ranges
                        if (couponStartDate <= currentDate && currentDate <= couponEndDate){
                            if (subdeptId==0){
                                this.setState({ discountAmount: couponAmount});
                                this.setState({ colorCode: 'green'});
                                this.setState({ couponErrorMessage: 'Your discount added.'});
                            }else {
                                //Checking Item List Has Or Not Sub Category Id
                                var cartItemList = this.props.cartItems;
                                var foundStatus=false;
                                cartItemList.forEach(function(_item, index) {
                                    if(_item.subdeptId==subdeptId){
                                        var couponAmount=responseJson.data[0].couponAmount;
                                        foundStatus=true;
                                    }
                                });

                                if (foundStatus==true){
                                    this.setState({ discountAmount: couponAmount});
                                    this.setState({ colorCode: 'green'});
                                    this.setState({ couponErrorMessage: 'Your discount added.'});
                                }else{
                                    this.setState({ colorCode: 'green'});
                                    this.setState({ couponErrorMessage: 'This coupon applied only for category wise product.'});
                                }
                            }
                        }else {
                            this.setState({ colorCode: 'red'});
                            this.setState({ couponErrorMessage: 'Your coupon date expired.'});
                        }
                    }else {
                         this.setState({ colorCode: 'red'});
                         this.setState({ couponErrorMessage: 'Your coupon not matched.'});
                    }
                });
        }else {
            this.setState({ colorCode: 'red'});
            this.setState({ couponErrorMessage: 'Coupon number is required.'});
        }
    };

    // End ----Coupon Checking Within Date Range And Sub Category Wise


    async postInvoice(data) {
        const headers = {
            "Content-Type": "application/x-www-form-urlencoded",
            "content-type": "multipart/form-data",
            "X-Requested-With": "XMLHttpRequest"
        };
        let response = await axios.post(
            API.orders,
            data,
            {
                headers: headers
            }
        );
        if (response.data.success) {
            console.log("Your review has been added successfully", response);
            this.setState({ orderId: response.data.data.id });
            this.setState({ order_id: response.data.data.id });
            this.sellDetails(response.data.data.id);
        } else {
            console.log(response);
        }
    }

    //Billing Information Save And Pass Data TO Paysenz
    SaveBillingOrder = (payment_reference, gateway) => {
        console.log(this.props.cartItems);
        const data = new FormData();
        let billingAdd = {
            first_name: this.state.first_name ? this.state.first_name : "",
            item_id:  this.props.cartItems[0].id?  this.props.cartItems[0].id : "",
            last_name: this.state.last_name ? this.state.last_name : "",
            phone: this.state.phone ? this.state.phone : "",
            email: this.state.email ? this.state.email : "",
            address_1: this.state.address ? this.state.address : "",
            city: this.state.city ? this.state.city : "",
            state: this.state.state ? this.state.state : "",
            country: this.state.country ? this.state.country : "",
            image: this.props.cartItems[0].image ? this.props.cartItems[0].image.medium  : this.props.cartItems[0].pictures[0] ? this.props.cartItems[0].pictures[0]:`${process.env.PUBLIC_URL}/assets/images/no-preview-image.png`,
            area_code: this.state.spincode ? this.state.spincode : "",
            division: this.state.division ? this.state.division : "",
            district: this.state.district ? this.state.district : "",
            upazilla: this.state.upazilla ? this.state.upazilla : ""
        };

        let x = new Date();
        let year = x.getFullYear();
        let month = x.getMonth() + 1;
        if (month < 10) month = `0${month}`;
        let date = x.getDate();
        if (date < 10) date = `0${date}`;
        let hour = x.getHours() + 1;
        if (hour < 10) hour = `0${hour}`;
        let min = x.getMinutes() + 1;
        if (min < 10) min = `0${min}`;
        let second = x.getSeconds() + 1;
        if (second < 10) second = `0${second}`;

        let InvoiceNo = `IN${year}${month}${date}${hour}${min}${second}`;
        data.append("SalesInvoice[invNo]", InvoiceNo);
        data.append("SalesInvoice[custId]", this.state.customerId?this.state.customerId:localStorage.getItem('custId'));
        data.append("SalesInvoice[billingAddress]", JSON.stringify(billingAdd));
        data.append("SalesInvoice[shippingAddress]", JSON.stringify(billingAdd));
        data.append("SalesInvoice[couponId]", 8);
        // data.append("SalesInvoice[shippingId]", data.shippingId);
        data.append("SalesInvoice[status]", 3);
        data.append("SalesInvoice[isEcommerce]", 1);
        data.append("SalesInvoice[totalQty]", this.props.cartItems ? this.props.cartItems.length : "");
        data.append("SalesInvoice[totalPrice]", this.props.total);
        data.append("SalesInvoice[totalDiscount]", this.props.discount+this.state.discountAmount);
        data.append("SalesInvoice[divisions]", this.state.division);
        data.append("SalesInvoice[districts]", this.state.district);
        data.append("SalesInvoice[upazilas]", this.state.upazilla);
        data.append("SalesInvoice[branchId]", this.state.branch?this.state.branch:'1');
        data.append("SalesInvoice[paymentId]", this.state.paymentStatus?this.state.paymentStatus:'0');
        this.postInvoice(data);
    };

    async postCartDetails(data) {
        let billingAdd = {
            first_name: this.state.first_name ? this.state.first_name : "",
            item_id:  this.props.cartItems[0].id?  this.props.cartItems[0].id : "",
            last_name: this.state.last_name ? this.state.last_name : "",
            phone: this.state.phone ? this.state.phone : "",
            email: this.state.email ? this.state.email : "",
            address_1: this.state.address ? this.state.address : "",
            city: this.state.city ? this.state.city : "",
            state: this.state.state ? this.state.state : "",
            country: this.state.country ? this.state.country : "",
            image: this.props.cartItems[0].image? this.props.cartItems[0].image.medium  : this.props.cartItems[0].pictures[0] ? this.props.cartItems[0].pictures[0]:`${process.env.PUBLIC_URL}/assets/images/no-preview-image.png`,
            area_code: this.state.spincode ? this.state.spincode : "",
            division: this.state.division ? this.state.division : "",
            district: this.state.district ? this.state.district : "",
            upazilla: this.state.upazilla ? this.state.upazilla : ""
        };
        let response = await axios.post(
            API.orders_multiple,
            data,
            {
                headers: ""
            }
        );
        if (response.data.success) {
            console.log("Cart Details Added", response);
            this.setState({ billing_address: billingAdd });
            this.setState({ showClickButoon: !this.state.showClickButoon }); //Open Paysenz Button
        } else {
            console.log(response);
        }
    }

    sellDetails = orderId => {
        if (orderId) {
            var cartItemList = this.props.cartItems;
            let cartData = new FormData();
            if (cartItemList.length > 0) {
                cartItemList.forEach(function(_item, index) {
                    let currentItemId='';
                    if(_item.variation_type==2){
                        currentItemId=_item.variation_id?_item.variation_id:'';
                    }else{
                        currentItemId=_item.id?_item.id:'';
                    }
                    cartData.append(`SalesDetails[${index}][salesId]`, orderId);
                    cartData.append(`SalesDetails[${index}][itemId]`, currentItemId);
                    cartData.append(`SalesDetails[${index}][qty]`, _item.qty.toString());
                    cartData.append(`SalesDetails[${index}][costPrice]`, _item.price?_item.price.toString():_item.cost_price.toString());
                    cartData.append(`SalesDetails[${index}][salesPrice]`, _item.sell_price.toString());
                    cartData.append(`SalesDetails[${index}][status]`, "1");
                    cartData.append(`SalesDetails[${index}][variation_id]`, _item.variation_id?_item.variation_id:'');
                });
                this.postCartDetails(cartData);
            }
        }
    };

    render() {
        var tokenCheck = localStorage.getItem("token");
        const { language } = this.props;
        const { branchInforamtion } = this.state;

        if (tokenCheck) {
        } else {
            this.props.history.push("/login");
        }
        const { cartItems, symbol, total, translate } = this.props;

        return (
            <div>
                {/*SEO Support*/}
                <Helmet>
                    <title>PlusPoint | CheckOut Page</title>
                    <meta name="description" content="" />
                </Helmet>
                {/*SEO Support End */}
                <Breadcrumb title={"Checkout"} />

                <section className="section-b-space">
                    <div className="container padding-cls">
                        <div className="checkout-page">
                            <div className="checkout-form">
                                <form>
                                    <div className="checkout row">
                                        <div className="col-lg-6 col-sm-12 col-xs-12">
                                            <div className="checkout-title">
                                                <span className="contact-title">{language == "en" ? "Contact Information" : "যোগাযোগের তথ্য"}</span>
                                                <span style={{ float: "right" }}>
                          {language == "en" ? "Already have an account ? " : "ইতিমধ্যে একটি একাউন্ট আছে ?"}
                                                    <a href="#">{language == "en" ? "Login" : "লগইন"}</a>{" "}
                        </span>
                                                <input type="text" name="mail" />
                                                <br></br>
                                                <br></br>
                                                <input type="checkbox" class="form-check-input" id="exampleCheck1" style={{ marginLeft: "1px" }} />
                                                <label style={{ marginLeft: "26px" }} class="form-check-label" for="exampleCheck1">
                                                    {language == "en"
                                                        ? " Keep me up to date on news and exclusive offer"
                                                        : "সংবাদ এবং এক্সক্লুসিভ অফারে আমাকে আপ টু ডেট রাখুন"}
                                                </label>
                                            </div>

                                            <div className="checkout-title">
                                                <span className="contact-title">{language == "en" ? "Shipping Address" : "শিপিং ঠিকানা"}</span>
                                            </div>
                                            <div className="row check-out">
                                                <div className="form-group col-md-6 col-sm-6 col-xs-12">
                                                    <input
                                                        type="text"
                                                        placeholder="First Name (Optional)"
                                                        name="first_name"
                                                        value={this.state.first_name}
                                                        onChange={event => this.setState({ first_name: event.target.value })}
                                                    />
                                                    {this.validator.message("first_name", this.state.first_name, "required|alpha")}
                                                </div>
                                                <div className="form-group col-md-6 col-sm-6 col-xs-12">
                                                    <input
                                                        type="text"
                                                        placeholder="Last Name"
                                                        name="last_name"
                                                        value={this.state.last_name}
                                                        onChange={e => this.setState({ last_name: e.target.value })}
                                                    />
                                                    {this.validator.message("last_name", this.state.last_name, "required|alpha")}
                                                </div>
                                                <div className="form-group col-md-6 col-sm-6 col-xs-12">
                                                    <input
                                                        type="text"
                                                        name="phone"
                                                        placeholder="Phone"
                                                        value={this.state.phone}
                                                        onChange={e => this.setState({ phone: e.target.value })}
                                                    />
                                                    {this.validator.message("phone", this.state.phone, "required|phone")}
                                                </div>
                                                <div className="form-group col-md-6 col-sm-6 col-xs-12">
                                                    <input
                                                        type="text"
                                                        name="email"
                                                        placeholder="Email"
                                                        value={this.state.email}
                                                        onChange={e => {
                                                            this.setState({ email: e.target.value });
                                                        }}
                                                    />
                                                    {this.validator.message("email", this.state.email, "required|email")}
                                                </div>
                                                <div className="form-group col-md-6 col-sm-12 col-xs-12">
                                                    <select name="country" disabled value={this.state.country}>
                                                        <option selected value="Bangladesh">
                                                            {language == "en" ? "Bangladesh" : "বাংলাদেশ"}
                                                        </option>
                                                    </select>
                                                    {this.validator.message("country", this.state.country, "required")}
                                                </div>
                                                <div className="form-group col-md-6 col-sm-12 col-xs-12">
                                                    <select name="division" value={this.state.division} onChange={this.onDivisionChangeHandler}>
                                                        <option value="">{language == "en" ? "Select Division" : "বিভাগ নির্বাচন করুন"}</option>
                                                        {division.map(div => (
                                                            <option value={div.id}>{language == "en" ? div.name : div.bn_name}</option>
                                                        ))}
                                                    </select>
                                                    {this.validator.message("division", this.state.division, "required")}
                                                </div>
                                                <div className="form-group col-md-6 col-sm-12 col-xs-12">
                                                    <select name="district" value={this.state.district} onChange={this.onDistrictChangeHandler}>
                                                        <option value="">{language == "en" ? "Select District" : "জেলা নির্বাচন করুন"}</option>
                                                        {_.filter(district, item => item.division_id == this.state.division).map(dist => (
                                                            <option value={dist.id}>{language == "en" ? dist.name : dist.bn_name}</option>
                                                        ))}
                                                    </select>
                                                    {this.validator.message("district", this.state.district, "required")}
                                                </div>
                                                <div className="form-group col-md-6 col-sm-12 col-xs-12">
                                                    <select name="upazilla" value={this.state.upazilla} onChange={this.onUpazillaChangeHandler}>
                                                        <option value="">{language == "en" ? "Select Upazilla" : "উপজেলা নির্বাচন করুন"}</option>
                                                        {_.filter(upazilla, item => item.district_id == this.state.district).map(dist => (
                                                            <option value={dist.id}>{language == "en" ? dist.name : dist.bn_name}</option>
                                                        ))}
                                                    </select>
                                                    {this.validator.message("upazilla", this.state.upazilla, "required")}
                                                </div>
                                                <div className="form-group col-md-6 col-sm-12 col-xs-12">
                                                    <input
                                                        type="text"
                                                        name="address"
                                                        placeholder="Address"
                                                        value={this.state.address}
                                                        onChange={e => this.setState({ address: e.target.value })}
                                                        placeholder="Street address"
                                                    />
                                                    {this.validator.message("address", this.state.address, "required|min:8|max:120")}
                                                </div>

                                                <div className="form-group col-md-6 col-sm-6 col-xs-12">
                                                    <input
                                                        type="text"
                                                        name="pincode"
                                                        placeholder="Zip Code"
                                                        value={this.state.spincode}
                                                        onChange={e => this.setState({ spincode: e.target.value })}
                                                    />
                                                    {/* {this.validator.message("pincode", this.state.pincode, "required|integer")} */}
                                                </div>

                                                <div style={{width:"100%",marginLeft: "1px",marginTop:"100px"}} className="row">
                                                    <div className="form-group col-md-6 col-sm-8 col-xs-12">
                                                        <input
                                                            type="text"
                                                            name="coupon"
                                                            placeholder="Use coupon get discount"
                                                            value={this.state.coupon}
                                                            onChange={e => this.setState({ coupon: e.target.value })}
                                                        />
                                                        <div className="srv-validation-message" style={{color: this.state.colorCode}}>
                                                            {this.state.couponErrorMessage}
                                                        </div>
                                                    </div>
                                                    <div className="form-group col-md-5 col-sm-4 col-xs-12">
                                                        <button style={{ color: "black" ,width: "183px",height: "43px" }} type="button"
                                                                className="btn-solid btn"
                                                                onClick={() => this.getDiscountClick()}>
                                                             Apply Coupon
                                                        </button>
                                                    </div>
                                                </div>

                                                {/*<div className="form-group col-md-12 col-sm-12 col-xs-12">*/}
                                                    {/*<select name="branch" value={this.state.branch} onChange={this.onBranchChangeHandler}>*/}
                                                        {/*<option value="">{language == "en" ? "Select Branch" : "Select Branch"}</option>*/}
                                                        {/*{branchInforamtion.map(branch => (*/}
                                                            {/*<option value={branch.id}>{language == "en" ? branch.name : branch.name}</option>*/}
                                                        {/*))}*/}
                                                    {/*</select>*/}
                                                    {/*{this.validator.message("branch", this.state.branch, "required")}*/}
                                                {/*</div>*/}
                                            </div>
                                            {/* <input type="checkbox" class="form-check-input" id="exampleCheck1" style={{ marginLeft: "1px" }} />
                      <label style={{ marginLeft: "26px" }} class="form-check-label" for="exampleCheck1">
                        {language == "en" ? "Save this information for next time." : "এই তথ্য পরবর্তী সময়ের জন্য সংরক্ষণ করুন।"}
                      </label> */}
                                        </div>
                                        <div className="col-lg-6 col-sm-12 col-xs-12">
                                            <div className="checkout-details">
                                                <div className="order-box">
                                                    <div className="title-box">
                                                        <div>
                                                            {language == "en" ? "Product" : "প্রোডাক্ট"} <span> {language == "en" ? "Total" : "মোট"}</span>
                                                        </div>
                                                    </div>
                                                    <ul className="qty">
                                                        {cartItems.map((item, index) => {
                                                            return (
                                                                <li key={index}>
                                                                    <Link to={`${process.env.PUBLIC_URL}/left-sidebar/product/${item.id}`}>
                                                                        <img
                                                                            className="checkout-image"
                                                                            src={item.image ? item.image.medium : item.pictures?item.pictures[0]:`${process.env.PUBLIC_URL}/assets/images/no-preview-image.png`}
                                                                            alt=""
                                                                        />
                                                                    </Link>
                                                                    <span
                                                                        style={{
                                                                            width: "183px",
                                                                            fontSize: "14px",
                                                                            float: "none",
                                                                            display: "inline-block",
                                                                            lineHeight: "initial",
                                                                            verticalAlign: "middle"
                                                                        }}
                                                                    >
                                                                        {language == "en" ? item.name : item.name_bd}
                                                                    </span>{" "}
                                                                    ×{" "}
                                                                    <span
                                                                        style={{
                                                                            verticalAlign: "middle",
                                                                            float: "none",
                                                                            width: "auto",
                                                                            fontSize: "16px",
                                                                            marginRight: "15px"
                                                                        }}
                                                                    >
                                                                         {item.qty}
                                                                    </span>{" "}
                                                                    <span
                                                                        style={{
                                                                            fontSize: "16px",
                                                                            width: "auto",
                                                                            verticalAlign: "middle"
                                                                        }}
                                                                    >
                                                                         {symbol} {item.sum}
                                                                    </span>
                                                                </li>
                                                            );
                                                        })}
                                                    </ul>
                                                    <ul className="sub-total">
                                                        <li>
                                                            {language == "en" ? "Subtotal" : "মোট পরিমাণ"}{" "}
                                                            <span style={{ fontWeight: "900" }} className="count">
                                                                {symbol}
                                                                {total}
                                                                </span>
                                                        </li>
                                                        <li>
                                                            {language == "en" ? "Shipping" : "শিপিং"}{" "}
                                                            <div className="shipping">
                                                                <div className="shopping-option">
                                                                    <input type="checkbox" name="free-shipping" id="free-shipping" />
                                                                    <label htmlFor="free-shipping">{language == "en" ? "Free Shipping" : "ফ্রি শিপিং"}</label>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>

                                                    <ul className="total">
                                                        <li style={{ fontWeight: "900" }}>
                                                            {language == "en" ? "Total" : "মোট"}{" "}
                                                            <span className="count" style={{ fontWeight: "900" }}>
                                                                {symbol}
                                                                {total-this.state.discountAmount}
                                                                </span>
                                                        </li>
                                                    </ul>
                                                </div>

                                                <div className="payment-box">
                                                    <div className="upper-box">
                                                        <div className="payment-options">
                                                            <ul>
                                                                <li>
                                                                    <div className="radio-option stripe" style={{marginBottom: "10px", textAlign: "Left", color: "red", display: "inline-block", fontWeight: "bold" }} >
                                                                        "Sorry for the current scenario risen by corona virus that we can't take any casn on delivery orders, So please do Paysenz payment as an advance payment for your orders"
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div className="radio-option paypal">
                                                                        <input type="radio" defaultChecked={true} name="payment-group" id="payment-1" onClick={() => this.checkhandle("paysenz")} />
                                                                        <label htmlFor="payment-1">
                                                                            Paysenz
                                                                            <span className="image">
                                                                                    <img src={`${process.env.PUBLIC_URL}/assets/images/paypal.png`} alt="" />
                                                                             </span>
                                                                        </label>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    {total !== 0 ? (
                                                        <div className="text-right">
                                                            {
                                                                //   this.state.payment === "stripe" ? (
                                                                //     this.state.cashbackButtonStatus ? (
                                                                //       <div className="col-lg-12">
                                                                //         <h3 className="loder_style" style={{ textAlign: "Left", color: "#000", display: "inline-block", fontWeight: "bold" }}>
                                                                //           Your Order Is Processing
                                                                //           <img
                                                                //               style={{ width: "10%", display: "inline-block", marginLeft: "20px" }}
                                                                //               src={`${process.env.PUBLIC_URL}/assets/images/icon/loding.gif`}
                                                                //           />
                                                                //         </h3>
                                                                //       </div>
                                                                // ) : (
                                                                //     <div>
                                                                //       <button type="button" className="btn-solid btn" onClick={() => this.CashOnDeliveryClick()}>
                                                                //         {language == "en" ? "Place Order" : "অর্ডার দিন"}
                                                                //       </button>
                                                                //     </div>
                                                                // )
                                                                // ) :
                                                                this.state.showClickButoon ? (
                                                                    <button type="button"
                                                                            className={this.state.disableStatus? "btn-solid btn pointerCheck" : "btn-solid btn"}
                                                                            onClick={() => this.PaysenzClick()}>
                                                                        Paysenz Order
                                                                    </button>
                                                                ) : (
                                                                    <Paysenz
                                                                        items={this.props.cartItems}
                                                                        orderTotal={this.state.orderTotals?this.state.orderTotals:this.props.total}
                                                                        symbol={this.props.symbol}
                                                                        billing={this.state}
                                                                    />
                                                                )}
                                                        </div>
                                                    ) : (
                                                        ""
                                                    )}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    cartItems: state.cartList.cart,
    symbol: state.data.symbol,
    total: getCartTotal(state.cartList.cart),
    discount: getTotalDiscount(state.cartList.cart),
    language: state.Intl.locale,
    userType: state.userType.userType
});

export default connect(mapStateToProps, { removeFromWishlist })(checkOut);
