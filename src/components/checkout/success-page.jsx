import React, {Component} from 'react';
import API from "../../config";
import {
    changeCurrency, changeUserType,
    removeAllCartItems,
} from "../../actions";
import connect from "react-redux/es/connect/connect";

class orderSuccess extends Component {
    constructor (props) {
        super (props)
        this.props.removeAllCartItems(); //Remove All Cart Items According TO Variables Name
    }
    componentDidMount() {
        if(this.props.match.params.orderid){
           this.InvoiceSend(this.props.match.params.orderid);
        }
        if(localStorage.getItem('payment_method')=="paysenz" && this.props.match.params.orderid){
            console.log(localStorage.getItem('payment_method'));
            this.InvoiceStatusUpdate(this.props.match.params.orderid);
            this.InvoiceSend(this.props.match.params.orderid);
        }
    }

    InvoiceStatusUpdate(orderid) {
        fetch(API.status_update, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
            },
            body: JSON.stringify({
                order_id: orderid,
                payment_status: 'payed',
            })
        }).then((Response) => Response.json())
            .then((result) => {
            console.log(result);
            console.log("Success");
            console.log(localStorage.removeItem('payment_method'));
        })
    }

    InvoiceSend(orderid) {
        fetch(API.checkout_success_email, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
            },
            body: JSON.stringify({
                id: orderid,
            })
        }).then((Response) => Response.json())
            .then((result) => {
            console.log(result);
            console.log("Success");
            })
    }

    render (){
        let orderid=this.props.match.params.orderid;
        let {userType}=this.props;
        return (

            <div>
                {removeAllCartItems}
                <section className="section-b-space light-layout">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="success-text">
                                    <i className="fa fa-check-circle" aria-hidden="true"></i>
                                    <h2>thank you</h2>
                                    <p>Payment Is Has Been Received Order Placed Successfully</p>
                                    {userType != "Guest" ? (
                                    <a href={`${process.env.PUBLIC_URL}/my-orders`} className="btn btn-success">Go to my
                                        orders</a>
                                    ) : (
                                      ''
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    language: state.Intl.locale,
    userType: state.userType.userType
});
export default connect(mapStateToProps, {removeAllCartItems,}) (orderSuccess);