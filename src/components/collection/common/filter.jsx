import React, { Component } from "react";
import { connect } from "react-redux";
import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";
import { SlideToggle } from "react-slide-toggle";
import API from "../../../config";

import { getBrands, getColors, getMinMaxPrice ,getsize} from "../../../services";
import { filterBrand, filterColor, filterPrice, filterDepartment,filterSize } from "../../../actions";

class Filter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      openFilter: false,
      departmentList: [],
      category: null,
      value: -1,
      productList: []
    };
    this.props.filterPrice("none");
  }
  componentDidMount() {
    this.fetchDepartment();
  }

  onChangePriceFilter = value => {    
    this.setState({ value: value });
    this.props.filterPrice(value);
  };

  closeFilter = () => {
    document.querySelector(".collection-filter").style = "left: -365px";
  };

  clickBrandHendle(event, brands) {
    var index = brands.indexOf(event.target.value);
    if (event.target.checked) brands.push(event.target.value);
    // push in array checked value
    /* alert("Unchecked");*/ else brands.splice(index, 1); // removed in array unchecked value

    this.props.filterBrand(brands);
  }
  clickSizeHandle(event, size) {
    var sizeMakeArray=[];
    var index = size.indexOf(event.target.value);
    if (event.target.checked){
      sizeMakeArray.push(event.target.value);
      this.props.filterSize(sizeMakeArray);
    }else{
      this.props.filterSize(sizeMakeArray);
    }
  }

  clickDepartmentHandle(event, dep) {
    let department;
    if (!Array.isArray(dep)) {
      department = [dep];
    } else {
      department = dep;
    }
    var index = department.indexOf(event.target.value);
    if (event.target.checked) department.push(event.target.value);
    else department.splice(index, 1);
    this.props.filterDepartment(department);
  }

  colorHandle(event, color) {
    var elems = document.querySelectorAll(".color-selector ul li");
    [].forEach.call(elems, function(el) {
      el.classList.remove("active");
    });
    event.target.classList.add("active");
    this.props.filterColor(color); 
    console.log("this.props.filterColor(color)");
    console.log(this.props.filterColor(color));
  }
  fetchDepartment() {
        fetch(API.department, {
          method: "GET",
          cache: "no-cache"
        }).then(res => res.json())
        .then(myJson => {
          var departmentList = myJson.data.slice(0, 10);
          this.setState({
            departmentList: departmentList
          });
        });
  }

  render() {
    const filteredBrands = this.props.filters.brand;
    const filteredColors = this.props.filters.color;
    const filteredSize = this.props.filters.size;
    var { departmentList } = this.state;
    var selectedDepartment = this.props.filters.department;
    var { language, symbol} = this.props;   
    
    var totalSizeSlice=(this.props.sizes.length)/2;
  
    return (
      <div className="collection-filter-block">
        {/* <ul className="parent-cat">
          <li><input type="checkbox" id="vehicle1" value="top" name="vehicle1" onClick={this.arrow}/> Top
            <ul className="child-cat" id="collapse-menu-new" style={{display: "none"}}>
              <li><input type="checkbox" id="myCheck" name="vehicle1" onClick={this.childArrow}/> Shirt
                <ul className="child-sub-cat" id="collapse-menu-child" style={{display: "none"}}>
                  <li><input type="checkbox" id="myCheck" name="vehicle1"/> Formal</li>
                  <li><input type="checkbox" id="myCheck" name="vehicle1"/> Casual</li>
                </ul>
              </li>
              <li><input type="checkbox" id="myCheck" name="myCheck" value="top"/> T-Shirt</li>
            </ul>
          </li>
        </ul> */}
        {/*brand filter start*/}
        <div className="collection-mobile-back">
          <span className="filter-back" onClick={e => this.closeFilter(e)}>
            back
          </span>
        </div>
        <div className="">
          <SlideToggle duration={100}>
            {({ onToggle, setCollapsibleElement }) => (
              <div className="collection-collapse-block">
                <h4 className="collapse-block-title" onClick={onToggle}>
                  {language === "en" ? "Collection" : "সংগ্রহ"}
                </h4>

                <div className="collection-filter-toggle" ref={setCollapsibleElement}>
                  {departmentList.map((department, index) => (
                    <div>
                      <div style={{ marginTop: "10px" }} className="custom-control custom-checkbox collection-filter-checkbox" key={index}>
                        <input
                          type="checkbox"
                          onClick={e => this.clickDepartmentHandle(e, selectedDepartment)}
                          value={department.department_slug}
                          defaultChecked={selectedDepartment.includes(department.department_slug) ? true : false}
                          className="custom-control-input"
                          id={department.department_name}            
                        />
                        <label className="custom-control-label" htmlFor={department.department_name}>
                          {" "}
                          {language === "en" ? department.department_name : department.department_name_bd}
                        </label>
                      </div>

                      <div className="collection-collapse-block-content" id="collapse-menu-new">
                        <div className="collection-brand-filter">
                          {department.subdepartment.slice(0, 30).map((subdepartment, index) => {                            
                            return (
                              <div className="custom-control custom-checkbox collection-filter-checkbox" key={index}>
                                <input
                                  type="checkbox"
                                  onClick={e => this.clickDepartmentHandle(e, selectedDepartment)}
                                  value={subdepartment.subdepartment_slug}
                                  defaultChecked={selectedDepartment.includes(subdepartment.subdepartment_slug) ? true : false}
                                  className="custom-control-input"
                                  id={subdepartment.subdepartment_name}
                                />
                                <label className="custom-control-label" htmlFor={subdepartment.subdepartment_name}>
                                  {" "}
                                  {language === "en" ? subdepartment.subdepartment_name : subdepartment.subdepartment_name_bd}
                                </label>
                                {subdepartment.subcategory.map(sub=>
                                  <div id="collapse-menu-child">
                                  <div className="custom-control custom-checkbox collection-filter-checkbox" key={index}>
                                    <input
                                      type="checkbox"
                                      onClick={e => this.clickDepartmentHandle(e, selectedDepartment)}
                                      value={sub.subcategory_name_slug}
                                      defaultChecked={selectedDepartment.includes(sub.subcategory_name_slug) ? true : false}
                                      className="custom-control-input"
                                      id={sub.subcategory_name}
                                    />
                                    <label className="custom-control-label" htmlFor={sub.subcategory_name}>
                                      {" "}
                                      {language === "en" ? sub.subcategory_name : sub.subcategory_name_bd}
                                    </label>
                                  </div>
                                  </div>
                                  )}
                              </div>
                            );
                          })}
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            )}
          </SlideToggle>
        </div>

        {/*price filter start here */}
        <SlideToggle whenReversedUseBackwardEase>
          {({ onToggle, setCollapsibleElement }) => (
            <div className="collection-collapse-block open">
              <h3 className="collapse-block-title" onClick={onToggle}>
                {language === "en" ? "price" : "মূল্য"}
              </h3>
              <div className="collection-collapse-block-content block-price-content" ref={setCollapsibleElement}>
                <div className="collection-brand-filter">
                  <div className="custom-control custom-checkbox collection-filter-checkbox">
                    <InputRange
                      formatLabel={value => {
                        if (value === -1) return "";
                        else return `${symbol}${value}`;
                      }}
                      steps={10}
                      maxValue={this.props.prices.max}
                      minValue={this.props.prices.min}
                      value={this.state.value}
                      onChange={this.onChangePriceFilter}
                    />
                  </div>
                </div>
              </div>
            </div>
          )}
        </SlideToggle>
        {/* size filter  */}
        <SlideToggle>
          {({ onToggle, setCollapsibleElement }) => (
            <div className="collection-collapse-block">
              <h3 className="collapse-block-title" onClick={onToggle}>
                {language === "en" ? "Size" : "Size"}
              </h3>
              <div className="collection-collapse-block-content" ref={setCollapsibleElement}>
                <div className="collection-brand-filter">
                      <div className="row">
                      
                        <div className="col-md-4">
                        {this.props.sizes.slice(0,totalSizeSlice).map((size, index) => {
                    return (
                          <div className="custom-control custom-checkbox collection-filter-checkbox" key={index}>
                            <input
                              type="checkbox"
                              value={size}
                              onClick={e => this.clickSizeHandle(e, size)}
                              className="custom-control-input"
                              id={size}
                            />
                            <label className="custom-control-label" htmlFor={size}>
                              {size}
                            </label>
                          </div>
                          );
                        })}
                        </div>

                        <div className="col-md-6">
                        {this.props.sizes.slice(totalSizeSlice,this.props.sizes.length).map((size, index) => {
                    return (
                          <div className="custom-control custom-checkbox collection-filter-checkbox" key={index}>
                            <input
                              type="checkbox"
                              value={size}
                              onClick={e => this.clickSizeHandle(e, size)}
                              className="custom-control-input"
                              id={size}
                            />
                            <label className="custom-control-label" htmlFor={size}>
                              {size}
                            </label>
                          </div>
                          );
                        })}
                        </div>
                        

                        {/* <div className="col-md-6">
                          <div className="custom-control custom-checkbox collection-filter-checkbox" key={size}>
                            <input
                              type="checkbox"
                              value={brand}
                              className="custom-control-input"
                              id={brand}
                            />
                            <label className="custom-control-label" htmlFor={size}>
                              S
                            </label>
                          </div>
                          
                        </div> */}
                      </div>                      
                   
                </div>
              </div>
            </div>
          )}
        </SlideToggle>
        {/* brands filter  */}
        <SlideToggle>
          {({ onToggle, setCollapsibleElement }) => (
            <div className="collection-collapse-block">
              <h3 className="collapse-block-title" onClick={onToggle}>
                {language === "en" ? "Brands" : "ব্রান্ডের"}
              </h3>
              <div className="collection-collapse-block-content" ref={setCollapsibleElement}>
                <div className="collection-brand-filter">
                  {this.props.brands.map((brand, index) => {
                    return (
                      <div className="custom-control custom-checkbox collection-filter-checkbox" key={index}>
                        <input
                          type="checkbox"
                          onClick={e => this.clickBrandHendle(e, filteredBrands)}
                          value={brand}
                          defaultChecked={filteredBrands.includes(brand) ? true : false}
                          className="custom-control-input"
                          id={brand}
                        />
                        <label className="custom-control-label" htmlFor={brand}>
                          {brand}
                        </label>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          )}
        </SlideToggle>
        {/*color filter start here*/}
        <SlideToggle>
          {({onToggle, setCollapsibleElement}) => (
              <div className="collection-collapse-block">
                <h3 className="collapse-block-title" onClick={onToggle}>colors</h3>
                <div className="collection-collapse-block-content" ref={setCollapsibleElement}>
                  <div className="color-selector">
                    <ul>
                      {this.props.colors.map((color, index) => {
                        return (
                            <li className={filteredColors &&filteredColors.includes(color) ? 'active' : ''}
                                value={color.toString()}
                                onClick={(e) => this.colorHandle(e, color)}
                                style={{ background: `${color.toLowerCase()}`, width: "25px", height: "25px", border: "1px solid #000" }}
                            ></li>
                            // <li className={color} title={color} onClick={(e) => this.colorHandle(e, color)} key={index}></li>
                      )
                      })}
                    </ul>
                  </div>
                </div>
              </div>
          )}
        </SlideToggle>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  brands: getBrands(state.data.products, ownProps.category),
  colors: getColors(state.data.products, ownProps.category),
  sizes: getsize(state.data.products, ownProps.category),
  prices: getMinMaxPrice(state.data.products, ownProps.category),
  filters: state.filters,
  language: state.Intl.locale,
  symbol: state.data.symbol
});

export default connect(mapStateToProps, {
  filterBrand,
  filterColor,
  filterPrice,
  filterDepartment,
  filterSize,
})(Filter);
