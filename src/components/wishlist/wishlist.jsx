import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import Breadcrumb from "../common/breadcrumb";
import { addToCartAndRemoveWishlist, removeFromWishlist } from "../../actions";
import DashboardLeft from "../pages/dashboard-left";

class WishListFrontend extends Component {
    changeQty = e => {
        this.setState({ quantity: parseInt(e.target.value) });
    };

    render() {
        const { Items, symbol, language } = this.props;
        return (
            <div>
                <Breadcrumb title={"My Wishlist"} />

                {/*Dashboard section*/}
                <section className="section-admin-space" style={{ PaddingTop: "0px!important" }}>
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="dashboard-right">
                                    {Items.length > 0 ? (
                                        <section className="wishlist-section section-b-space">
                                            <div className="container">
                                                <div className="row">
                                                    <div className="col-sm-12">
                                                        <table className="table cart-table order-table-responsive">
                                                            <thead>
                                                            <tr className="table-head">
                                                                <th scope="col">{language === "en" ? "image" : "চিত্র"}</th>
                                                                <th scope="col">{language === "en" ? "product name" : "পণ্যের নাম"}</th>
                                                                <th scope="col">{language === "en" ? "price" : "মূল্য"}</th>
                                                                <th scope="col">{language === "en" ? "availability" : "প্রাপ্যতা"}</th>
                                                                <th scope="col">{language === "en" ? "action" : "কর্ম"}</th>
                                                            </tr>
                                                            </thead>
                                                            {Items.map((item, index) => {
                                                                return (
                                                                    <tbody key={index}>
                                                                    <tr>
                                                                        <td>
                                                                            <Link to={`${process.env.PUBLIC_URL}/left-sidebar/product/${item.id}`}>
                                                                                <img src={item.pictures[0] ? item.pictures[0] :`${process.env.PUBLIC_URL}/assets/images/no-preview-image.png`} alt="" />
                                                                            </Link>
                                                                        </td>
                                                                        <td>
                                                                            <Link to={`${process.env.PUBLIC_URL}/left-sidebar/product/${item.id}`}>{item.name}</Link>
                                                                            <div className="mobile-cart-content row">
                                                                                <div className="col-xs-3">
                                                                                    <p>{language === "en" ? "in stock" : "মজুদ"}</p>
                                                                                </div>
                                                                                <div className="col-xs-3">
                                                                                    <h2 className="td-color">
                                                                                        {symbol}
                                                                                        {item.price - (item.price * item.discount) / 100}
                                                                                        <del>
                                                <span className="money">
                                                  {symbol}
                                                    {item.price}
                                                </span>
                                                                                        </del>
                                                                                    </h2>
                                                                                </div>
                                                                                <div className="col-xs-3">
                                                                                    <h2 className="td-color">
                                                                                        <a
                                                                                            href="javascript:void(0)"
                                                                                            className="icon"
                                                                                            onClick={() => this.props.removeFromWishlist(item)}
                                                                                        >
                                                                                            <i className="fa fa-times"></i>
                                                                                        </a>
                                                                                        <a
                                                                                            href="javascript:void(0)"
                                                                                            className="cart"
                                                                                            onClick={() => this.props.addToCartAndRemoveWishlist(item, 1)}
                                                                                        >
                                                                                            <i className="fa fa-shopping-cart"></i>
                                                                                        </a>
                                                                                    </h2>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <h2>
                                                                            {symbol}
                                                                            {item.salePrice - (item.salePrice * item.discount) / 100}
                                                                                <del>
                                            <span className="money">
                                            {symbol}
                                            {item.salePrice}
                                            </span>
                                                                                </del>
                                                                            </h2>
                                                                        </td>
                                                                        <td>
                                                                            <p>{language === "en" ? "in stock" : "মজুদ"}</p>
                                                                        </td>
                                                                        <td>
                                                                            <a href="javascript:void(0)" className="icon" onClick={() => this.props.removeFromWishlist(item)}>
                                                                                <i className="fa fa-times"></i>
                                                                            </a>
                                                                            <a
                                                                                href="javascript:void(0)"
                                                                                className="cart"
                                                                                onClick={() => this.props.addToCartAndRemoveWishlist(item, 1)}
                                                                            >
                                                                                <i className="fa fa-shopping-cart"></i>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                );
                                                            })}
                                                        </table>
                                                    </div>
                                                </div>
                                                <div className="row wishlist-buttons">
                                                    <div className="col-12">
                                                        <Link to={`${process.env.PUBLIC_URL}/products/top`} className="btn btn-solid btn-check">
                                                            {language === "en" ? "continue shopping" : "কেনাকাটা চালিয়ে যান"}
                                                        </Link>
                                                        <Link to={`${process.env.PUBLIC_URL}/checkout`} className="btn btn-solid btn-check">
                                                            {language === "en" ? "check out" : "চেক আউট"}
                                                        </Link>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    ) : (
                                        <section className="cart-section section-b-space">
                                            <div className="container">
                                                <div className="row">
                                                    <div className="col-sm-12">
                                                        <div>
                                                            <div className="col-sm-12 empty-cart-cls text-center">
                                                                <img src={`${process.env.PUBLIC_URL}/assets/images/empty-wishlist.png`} className="img-fluid mb-4" alt="" />
                                                                <h3>
                                                                    <strong>{language === "en" ? "WhishList is Empty" : "উইশলিস্টটি খালি"}</strong>
                                                                </h3>
                                                                <h4>
                                                                    {language === "en" ? "Explore more shortlist some items." : "কিছু শর্টলিস্ট কিছু আইটেম এক্সপ্লোর করুন।"}
                                                                </h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}
const mapStateToProps = state => ({
    Items: state.wishlist.list,
    symbol: state.data.symbol,
    language: state.Intl.locale
});

export default connect(mapStateToProps, { addToCartAndRemoveWishlist, removeFromWishlist })(WishListFrontend);
