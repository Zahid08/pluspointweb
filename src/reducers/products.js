import { FETCH_SINGLE_PRODUCT, CHANGE_CURRENCY, RECEIVE_PRODUCTS ,PRODUCTSSEARCH, PRODUCTSSEARCHFLUSH } from "../constants/ActionTypes";

const initialState = {
  products: [],
  symbol: "৳",
  product_details: [],
  search:[]
};

const productReducer = (state = initialState, action) => {
  switch (action.type) {
    case RECEIVE_PRODUCTS:
      return { ...state, products: action.products };
    case FETCH_SINGLE_PRODUCT:
      if (state.products.findIndex(product => product.id === action.productId) !== -1) {
        const singleItem = state.products.reduce((itemAcc, product) => {
          return product;
        }, []);
        return { ...state, product_details: singleItem };
      }
    case CHANGE_CURRENCY:
      return { ...state, symbol: action.symbol };
    case PRODUCTSSEARCH:
      return { ...state, search: state.products.filter(e => (((e.name).toUpperCase()).indexOf(action.search) > -1) || (((e.description).toUpperCase()).indexOf(action.search) > -1))};
    case PRODUCTSSEARCHFLUSH:
      return { ...state, search:[] };
    default:
      return state;
  }
};
export default productReducer;
